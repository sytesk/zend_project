<?php

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'testing'));

// Ensure library/ is on include_path
$config = parse_ini_file(APPLICATION_PATH . '/configs/application.ini', 'staging');

$pathsDir = $config['production']['app.paths'];

$paths = array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
);
foreach ($pathsDir as $caminho) {
    if(is_dir($caminho)){
        $paths[] = $caminho;
    }
}


// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, $paths));

require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance();
