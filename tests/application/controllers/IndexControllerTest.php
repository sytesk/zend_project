<?php

class IndexControllerTest extends Zend_Test_PHPUnit_ControllerTestCase
{
    
    public function setUp()
    {
        $this->bootstrap = new Zend_Application(APPLICATION_ENV, APPLICATION_PATH . '/configs/application.ini');
        parent::setUp();
        
        $this->fazerLogin();
    }
    
    private function fazerLogin(){
        
        $auth = new Zend_Session_Namespace('auth');
                
        $auth->usuario = "usuarioteste";
        $auth->nome = "Usuário teste unitário";
        $auth->email = "teste@teste.com.br";
        $auth->departamento = "TI";
        $auth->gestor = "teste";
        $auth->entidade = "Sede";
        $auth->is_root = false;
        $auth->id_usuario = 1;
        
    }
    
    protected function assertPreConditions()
    {
       
    }
    
    protected function assertPostConditions()
    {
       
    }
    
    public function testIndexAction() {
        $this->dispatch('/');
        $this->assertController('index');
        $this->assertAction('index');
    }
    
    public static function tearDownAfterClass()
    {
       
    }

}

