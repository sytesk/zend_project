
CREATE SCHEMA acl;
CREATE SCHEMA auditoria;

ALTER SCHEMA acl OWNER TO postgres;



--
-- TOC entry 141 (class 1259 OID 16562)
-- Dependencies: 6
-- Name: action; Type: TABLE; Schema: acl; Owner: postgres; Tablespace: 
--

CREATE TABLE auditoria.tabela (
    id serial NOT NULL,
    nome character varying(150),
	CONSTRAINT pk_auditoria_tabela PRIMARY KEY (id)
);

CREATE TABLE acl.action (
    id serial NOT NULL,
    id_controller integer,
    nome character varying(30),
    descricao character varying(60),
	CONSTRAINT pk_acl_action PRIMARY KEY (id)
);

ALTER TABLE acl.action OWNER TO postgres;

CREATE TABLE acl.module (
    id serial NOT NULL,
    nome character varying(60),
    descricao character varying(60),
    CONSTRAINT pk_acl_module PRIMARY KEY (id)
);



CREATE TABLE acl.controller (
    id serial NOT NULL,
    id_module integer,
    nome character varying(100),
	descricao character varying(60),
    CONSTRAINT pk_acl_controller PRIMARY KEY (id),
    CONSTRAINT fk_acl_controller_module FOREIGN KEY (id_module)
      REFERENCES acl.module (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
);


ALTER TABLE acl.controller OWNER TO postgres;

CREATE TABLE acl.perfil (
    id serial NOT NULL,
    nome character varying(50),
	CONSTRAINT pk_acl_perfil PRIMARY KEY (id)
);

ALTER TABLE acl.perfil OWNER TO postgres;


CREATE TABLE acl.usuario
(
  id serial NOT NULL,
  id_perfil integer,
  nome character varying(60),
  email character varying(50),
  usuario character varying(50),
  senha character varying(50),
  gestor character varying(60),
  entidade character varying(60),
  departamento character varying(40),
  ativo boolean,
  CONSTRAINT pk_acl_usuario PRIMARY KEY (id)
);

ALTER TABLE acl.usuario OWNER TO postgres;


CREATE TABLE acl.permissao (
    id serial NOT NULL,
    id_action integer,
    id_usuario integer,
    id_perfil integer,
    allowed boolean,
	CONSTRAINT pk_acl_permissao PRIMARY KEY (id),
	CONSTRAINT fk_acl_permissao_action FOREIGN KEY (id_action)
      REFERENCES acl.action (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT fk_acl_permissao_usuario FOREIGN KEY (id_usuario)
      REFERENCES acl.usuario (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION  
)WITH (OIDS=FALSE);


ALTER TABLE acl.permissao OWNER TO postgres;



CREATE TABLE config (
    id serial NOT NULL,
    parametro character varying(50),
    valor text,
	CONSTRAINT pk_config PRIMARY KEY (id)
);


ALTER TABLE public.config OWNER TO postgres;




CREATE TABLE status (
    id serial NOT NULL,
    codigo integer,
    tabela character varying(50),
    nome character varying(60),
	CONSTRAINT pk_status PRIMARY KEY (id)
);


ALTER TABLE public.status OWNER TO postgres;


-- Insert acl.perfil

TRUNCATE TABLE acl.perfil;

INSERT INTO acl.perfil 
(nome)
VALUES 
('Administrador');

-- Insert Module
TRUNCATE TABLE acl.module;

INSERT INTO acl.module 
(id,nome,descricao)
VALUES 
(1,'default','Módulo Padrão'),
(2,'acl','ACL');

-- Insert acl.controller

TRUNCATE TABLE acl.controller;

INSERT INTO acl.controller 
(id,id_module,nome,descricao)
VALUES 
(1,1,'perfil','Perfil'),
(2,1,'usuario','Usuário');

-- Insert acl.action

INSERT INTO acl.action 
(id_controller,nome,descricao)
VALUES 
(1,'index','Listar'),
(1,'inserir','Inserir'),
(1,'alterar','Alterar'),
(1,'excluir','Excluir'),
(2,'index','Listar'),
(2,'inserir','Inserir'),
(2,'alterar','Alterar'),
(2,'excluir','Excluir');


 CREATE OR REPLACE FUNCTION retira_acentuacao(p_texto text)  
  RETURNS text AS  
 $BODY$  
 Select translate($1,  
 'áàâãäåaaaÁÂÃÄÅAAAÀéèêëeeeeeEEEÉEEÈìíîïìiiiÌÍÎÏÌIIIóôõöoooòÒÓÔÕÖOOOùúûüuuuuÙÚÛÜUUUUçÇñÑýÝ',  
 'aaaaaaaaaAAAAAAAAAeeeeeeeeeEEEEEEEiiiiiiiiIIIIIIIIooooooooOOOOOOOOuuuuuuuuUUUUUUUUcCnNyY'   
  );  
 $BODY$  
 LANGUAGE sql VOLATILE  
 COST 100;