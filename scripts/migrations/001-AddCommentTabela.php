<?php



class AddCommentTabela extends Akrabat_Db_Schema_AbstractChange{
    
    public function up() {
        $sql ="comment on table auditoria.tabela is 'Armazena os nomes das tabelas que tem registro gravado em auditoria.log';";
        $this->_db->query($sql);
        
    }

    public function down() {
        
        $sql ="comment on table auditoria.tabela is NULL;";
        $this->_db->query($sql);
    }

}
