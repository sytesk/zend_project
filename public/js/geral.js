/**
 * Funcoes globais para a aplicacao
 */
var Geral = {

    
    efeitos : function () {
		/* Botoes */
		$('.botao').button();
		
		$('input[type=submit], input[type=button], button').button();

		$('.botao.icone-adicionar').button({ icons: { primary: 'ui-icon-adicionar' } });
		$('.botao.icone-ativar').button({ icons: { primary: 'ui-icon-ativar' } });
		$('.botao.icone-desativar').button({ icons: { primary: 'ui-icon-desativar' } });
		$('.botao.icone-editar').button({ icons: { primary: 'ui-icon-editar' } });
		$('.botao.icone-excluir').button({ icons: { primary: 'ui-icon-excluir' } });
		$('.botao.icone-grupo').button({ icons: { primary: 'ui-icon-grupo' } });
		$('.botao.icone-imprimir').button({ icons: { primary: 'ui-icon-imprimir' } });
		$('.botao.icone-importar').button({ icons: { primary: 'ui-icon-importar' } });
		$('.botao.icone-listar').button({ icons: { primary: 'ui-icon-listar' } });
		$('.botao.icone-marcador').button({ icons: { primary: 'ui-icon-marcador' } });
		$('.botao.icone-salvar').button({ icons: { primary: 'ui-icon-salvar' } });
		$('.botao.icone-verificar').button({ icons: { primary: 'ui-icon-verificar' } });
		$('.botao.icone-visualizar').button({ icons: { primary: 'ui-icon-visualizar' } });
		$('.botao.icone-voltar').button({ icons: { primary: 'ui-icon-voltar' } });
		$('.botao.icone-pesquisar').button({ icons: { primary: 'ui-icon-pesquisar' } });
                $('.botao.icone-desfazer').button({ icons: { primary: 'ui-icon-desfazer' } });
		
		/* Paginação */
		$('div.paging span a, div.paging span.current').button();
		$('div.paging span a.prev, div.paging span.prev').button({ icons: { primary:'ui-icon-voltar' } });
		$('div.paging span a.next, div.paging span.next').button({ icons: { secondary:'ui-icon-proximo' } });
		
		/* Campos */
		$('input, textarea, select').addClass("ui-corner-all");
		
		/* Tabs e Accordion */
		$( ".tabs" ).tabs();
		$( ".accordion" ).accordion();
		
		/* Tabela */
		$('table').addClass('ui-corner-all');
		
		/* Máscaras */
                
		$('.timepicker').mask('00:00');
		$('.datepicker').mask('00/00/0000', {placeholder: "__/__/____"})
                               .change(function(){
                                   var re = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
                                   if(!re.test($(this).val())){
                                       $(this).val("");
                                       alert("Data inválida.");
                                   }
                               });
		
		$('.cep').mask('00000-000');
		$('.telefone').mask('(00) 0000-0000');
		$('.cpf').mask('000.000.000-00');
                $('.cnpj').mask('00.000.000/0000-00');
                
                $(".numeric").mask('000000000000000000000');
               
              
		/* Datepicker */
		$('.datepicker').datepicker({
			changeMonth: true,
			changeYear: true,
			showOn: 'button',
			buttonImage: "/img/icones/calendario.png",
			buttonImageOnly: true,
                        dateFormat: 'dd/mm/yy',
                        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                        nextText: 'Próximo',
                        prevText: 'Anterior'
		});
		$('.ui-datepicker-trigger').attr({title: 'Calendário', alt: 'Calendário'});
		
		/* Timepicker */
		$('.timepicker').after('<img class="timepicker-icone" src="/img/icones/relogio.png" alt="Autocomplete" title="Hora" />');
		
		/* Autocomplete */
		$('.autocomplete').after('<img class="autocomplete-icone" src="/img/icones/autocomplete.png" alt="Autocomplete" title="Autocomplete" />');
		
		 /*Personalização do jAlert*/
                $.fn.jAlert.defaults.confirmQuestion = 'Tem certeza que deseja fazer essa operação?';
                $.fn.jAlert.defaults.confirmBtnText = 'Sim';
                $.fn.jAlert.defaults.denyBtnText = 'Não';
		
		/* Form Input Error*/
		/*
		$('.form-error').after('<img class="form-error-icon" src="../img/icones/error.png" alt="Erro" title="Erro" />');
		*/
	},
	
	
};

$(document).ready(function(){
    Geral.efeitos();
});