function Acl() {
    
    var self = this;
    
    this.controlles = {};
    
    this.init = function(){
        
        $("#select-module").change(function(){
            
            var valor = $(this).val();
            self.loadControllers(valor);
        });
        
        self.loadControllers($("#select-module").val());
    },
    
    
           
   this.loadControllers = function(module){
       
       $("#select-controller").html('');
       $.each(self.controlles,function(i,r){
            if(r.id_module == module){
                $("#select-controller").append('<option value="'+r.id+'">'+r.descricao+'</option>');
            }
        });
       
   }        
    
}


