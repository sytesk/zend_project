
function AppCadastro() {
        
        var self = this;
        
        this.controller = '';
        this.module = '';
        this.idForm = 'form-cadastro';
        
        this.init = function(){
           
           //inicializa o form
           $('#'+self.idForm).ajaxForm({
                beforeSubmit:  self.beforeSave,
                success:       self.afterSave,
                error:self.ajaxFormError,
                dataType:  'json',
                type:'post'
                
           }); 
           
           
        },
                
             
        this.beforeSave = function(formData, jqForm, options) { 
            
            //var queryString = $.param(formData); 
            
            $("#div-form").LoadingOverlay("show");
            
            return true; 
        },         
                
        this.afterSave = function(json, statusText, xhr, $form)  { 
            
            $("#div-form").LoadingOverlay("hide", true);
            
            $.jAlert({
                'title': 'Aviso',
                'content': json.msg.txt,
                'theme': (json.msg.cod == 0 ? 'green' : 'red'),
                'btns': { 'text': 'fechar' }
            });
            
            if(json.msg.cod == 0){
                    setTimeout(function(){location.reload();},2000);
            }
            
        },         
        
        this.ajaxFormError = function(objectResponse,cd,invalidJSON){
            //console.log(objectResponse);
            
            $("#div-form").LoadingOverlay("hide", true);
            
            $.jAlert({
                'title': 'Erro',
                'content': objectResponse.responseText,
                'theme': 'red',
                'btns': { 'text': 'close' }
            });
            
        },     
        
        this.editar = function(id){
            
            $("#div-lista").LoadingOverlay("show");
            
          
           $("#div-form").load((self.module != '' ? "/"+self.module : "")+'/'+self.controller+'/form/id/'+id,
                                function(){
                                      self.mostraDiv('div-form'); 
                                      $("#div-lista").LoadingOverlay("hide", true);
                                });
            
        },
        
        this.novoRegistro = function () {
            
           $("#div-lista").LoadingOverlay("show");
            
          
           $("#div-form").load((this.module != '' ? "/"+self.module : "")+'/'+self.controller+'/form',
                                function(){
                                      self.mostraDiv('div-form'); 
                                      $("#div-lista").LoadingOverlay("hide", true);
                                });

        },
        
        this.excluir = function(id){
            
            $.fn.jAlert.defaults.confirmQuestion = "Tem certeza que deseja excluir esse registro?";
            confirm(function (e, btn) {
                $("#div-lista").LoadingOverlay("show");
                
                $.ajax({
                    url:(this.module != '' ? "/"+self.module : "")+'/'+self.controller+'/excluir',
                    data:{id:id},
                    dataType:"json",
                    type:"post",
                    success:function(json){
                        
                        $("#div-lista").LoadingOverlay("hide", true);
                        
                        $.jAlert({
                            'title': 'Aviso',
                            'content': json.msg.txt,
                            'theme': (json.msg.cod == 0 ? 'green' : 'red'),
                            'btns': { 'text': 'close' }
                        });
                        
                        if(json.msg.cod == 0){
                            setTimeout(function(){location.reload();},2000);
                        }
                        
                    },
                    error:function(ab,cd,InvalidJSON){
                        
                        $("#div-lista").LoadingOverlay("hide", true);
                    }
                });
            },
            function (e, btn) {

            });
            
        },
        
        this.pesquisar = function(){
           
            this.mostraDiv('div-lista');
        };
        
        this.mostraDiv = function(divName){
            $("#div-lista,#div-form").hide();

            $("#"+divName).show();
        },
        
        this.chageMask = function($checkBox,$textField,objectMask){
            
            var evalStr = '';
            i = 0; 
            $.each(objectMask, function( tipoMask, correntMask ) {
                 evalStr+= 'if($(this).val() == "'+tipoMask+'"){$textField.mask("'+correntMask+'");}';
                 i++;
            });
           
            $checkBox.change(function(){
               $textField.unmask();
               if($textField.val() == ''){
                  $textField.val($textField.attr("cache"));
               }
               eval(evalStr);
               
            });
            
            $checkBox.each(function(){
                if(typeof $(this).attr('checked') != 'undefined'){
                    $(this).change();
                }
            });
        }

       
}

