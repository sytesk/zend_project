<?php

/*
 * Provê métodos de autenticação com A.D.
 */

ini_set('memory_limit', '200M');

class APP_Ad {

    /**
     *
     * @var string host que disponibiliza o servico AD.
     */
    var $host = '';

    /**
     *
     * @var int porta utilizada pelo servico AD.
     */
    var $port = '';

    /**
     *
     * @var string local onde estao definidos os usuarios e perfil.
     */
    var $baseDn = '';

    /**
     *
     * @var string usuario para conexao com o servico AD.
     */
    var $user = '';

    /**
     *
     * @var string senha do usuario para conexao com o servico AD.
     */
    var $pass = '';

    /**
     *
     * @var string atributo utilizado para autenticacao no servico AD.
     */
    var $userIdType = 'sAMAccountName';

    /**
     *
     * @var string atributo utilizado para realizar uma busca pelo nome distinto
     */
    var $distinguishedName = 'distinguishedName';

    /**
     *
     * @var string atributo utilizado para saber se o usuario esta ativo
     */
    var $userAccountControl = 'userAccountControl';

    /**
     *
     * @var resource conexao com servico AD.
     */
    var $ds;

    /**
     *
     * @var array Atributos que serao utilizados pela aplicacao
     */
    var $atributosValidos = array(
        'cn',
        'st',
        'title',
        'distinguishedname',
        'displayname',
        'memberof',
        'department',
        'proxyaddresses',
        'name',
        'useraccountcontrol',
        'samaccountname',
        'samaccounttype',
        'userprincipalname',
        'manager',
        'mailnickname',
        'dn',
        'directreports'
    );

    const USUARIO_ATIVO = 512;
    const USUARIO_INATIVO = 514;
    const USUARIO_SENHA_NUNCA_EXPIRA = 66048;
    const USUARIO_SENHA_SEMPRE_EXPIRA = 66050;

    var $entidades = array(
        'filial' => 'Filial',
        'Matriz' => 'Matriz'
    );
	
    private static $instance = null;
	
	public static $dadosAd = array(
		'ad_servidor' => 'servidor.com.br',
		'ad_porta' => '389',
		'ad_base_dn' => 'ou=Usuarios,dc=matriz,dc=ce,dc=empresa,dc=com,dc=br',
		'ad_usuario' => 'cn=Sistema de Banco de Horas,ou=ContasDeServicos,dc=matriz,dc=ce,dc=empresa,dc=com,dc=br',
		'ad_senha' => 'senha',
	);

    /**
     * Abre a conexao com o servico AD
     */
    function __construct($host = '', $port = '', $baseDn = '', $user = '', $pass = '') {
		
		$this->host = self::$dadosAd['ad_servidor'];
        $this->port = self::$dadosAd['ad_porta'];
        $this->baseDn = self::$dadosAd['ad_base_dn'];
        $this->user = self::$dadosAd['ad_usuario'];
        $this->pass = self::$dadosAd['ad_senha'];
		
        //$this->host = $host;
        //$this->port = $port;
        //$this->baseDn = $baseDn;
        //$this->user = $user;
        //$this->pass = $pass;

        $this->connect();
    }

    function __destruct() {
        ldap_close($this->ds);
    }

    function connect() {
        $this->ds = ldap_connect($this->host, $this->port);
        ldap_set_option($this->ds, LDAP_OPT_PROTOCOL_VERSION, 3);

        return ldap_bind($this->ds, $this->user, $this->pass);
    }

    /**
     * Recupera as informacoes de uma arvore do servico AD.
     *
     * @param string $value
     *            valor a ser comparado com o atributo definido em $userIdType.
     * @param string $baseDn
     *            local onde estao definidos os usuarios e perfil.
     * @return array as informacoes da arvore do AD.
     */
    function find($value = '*', $baseDn = '') {
        $cacheKey = "{$this->userIdType}={$value}";
        $baseDn = (empty($baseDn)) ? $this->baseDn : $baseDn;

        if (!$entries = $this->getFromCache($cacheKey)) {
            $r = ldap_search($this->ds, $baseDn, $this->userIdType . '=' . $value, $this->atributosValidos);

            if ($r) {
                ldap_sort($this->ds, $r, 'sn');
                $entries = ldap_get_entries($this->ds, $r);
                $this->writeCache($cacheKey, $entries);
            }
        }

        return $entries;
    }

    /**
     * Realiza a autenticacao de um usuario com base no servico AD.
     *
     * @param string $username
     *            o nome do usuario.
     * @param string $password
     *            a senha do usuario.
     * @return boolean se o usuario foi autenticado ou nao.
     */
    function auth($username, $password) {
        $result = $this->find($username);

        if (isset($result [0])) {
            if (@ldap_bind($this->ds, $result [0] ['dn'], $password)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Lista os setores disponiveis no AD
     * @$filtro string filtro a ser considerado
     *
     * @return array Lista com os setores em ordem alfabetica
     */
    public function findDepts($filtro = null) {
        $setores = array();
        $info = $this->find();

        for ($i = 0; $i < $info ['count']; $i ++) {
            if (isset($info [$i] ['department'])) {
                for ($j = 0; $j < $info [$i] ['department'] ['count']; $j ++) {

                    if (isset($filtro) && !empty($filtro)) {
                        if (strpos(strtolower($info [$i] ['department'] [$j]), strtolower($filtro)) !== false) {
                            $setores [$info [$i] ['department'] [$j]] = $info [$i] ['department'] [$j];
                        }
                    } else {
                        $setores [$info [$i] ['department'] [$j]] = $info [$i] ['department'] [$j];
                    }
                }
            }
        }

        sort($setores);
        return $setores;
    }

    /**
     * Lista os usuarios disponiveis no AD
     * @$filtro string filtro a ser considerado
     *
     * @return array Lista com os usuarios no formato nick => nome
     */
    public function findUsers($filtro = null) {
        $usuarios = array();
        $info = $this->find();

        for ($i = 0; $i < $info ['count']; $i ++) {
            $typeId = $info [$i] [strtolower($this->userIdType)] [0];
            $userAccountControl = strtolower($this->userAccountControl);

            if (!isset($info[$i][$userAccountControl][0]) || $info[$i][$userAccountControl][0] == self::USUARIO_INATIVO) {
                continue;
            }

            if (isset($filtro) && !empty($filtro)) {
                if (strpos(strtolower($info [$i] ['cn'] [0]), strtolower($filtro)) !== false) {
                    $usuarios [$typeId] = $info [$i] ['cn'] [0];
                }
            } else {
                $usuarios [$typeId] = $info [$i] ['cn'] [0];
            }
        }

        return $usuarios;
    }

    /**
     * Obtem o usuario que possui aquele username
     *
     * @param $username string
     *
     * @return array
     */
    public function findByUsername($username) {
        if (empty($username)) {
            return null;
        }

        $info = $this->find($username);

        if (isset($info [0])) {
            $info [0] ['manager'] [0] = (isset($info [0] ['manager'] [0]) ? $info [0] ['manager'] [0] : '');
            $info [0] ['userprincipalname'] [0] = (isset($info [0] ['userprincipalname'] [0]) ? $info [0] ['userprincipalname'] [0] : '');
            $info [0] ['department'] [0] = (isset($info [0] ['department'] [0]) ? $info [0] ['department'] [0] : '');
            $info [0] ['mail'] [0] = (isset($info [0] ['mail'] [0]) ? $info [0] ['mail'] [0] : $info [0] ['userprincipalname'] [0]);
            $entidade = $this->obterEntidade($info);

            return array(
                'nome' => $info [0] ['cn'] [0],
                'email' => $info [0] ['mail'] [0],
                'usuario' => $info [0] ['samaccountname'] [0],
                'departamento' => $info [0] ['department'] [0],
                'manager' => $info [0] ['manager'] [0],
                'entidade' => $entidade
            );
        }

        return null;
    }

    public static function getInstance() {
        if (null === self::$instance) {
			$conf = array('Configuracoes' => self::$dadosAd);
            self::$instance = new APP_Ad($conf ['Configuracoes'] ['ad_servidor'], $conf ['Configuracoes'] ['ad_porta'], $conf ['Configuracoes'] ['ad_base_dn'], $conf ['Configuracoes'] ['ad_usuario'], $conf ['Configuracoes'] ['ad_senha']);
        }

        return self::$instance;
    }

    /**
     * Obtém o gerente de um determinado usuário
     *
     * @param string $username
     * @return string O username do gerente
     */
    public function getManager($username) {
        $user = $this->find($username);
        $manager = isset($user [0] ['manager'] [0]) ? $user [0] ['manager'] [0] : '';

        $info = $this->findBy($manager, $this->distinguishedName);
        $userTypeId = strtolower($this->userIdType);

        return (isset($info [0] [$userTypeId] [0])) ? $info [0] [$userTypeId] [0] : '';
    }

    /**
     *
     * @param unknown $username
     * @param string $list
     * @return multitype:NULL
     */
    public function getSubordinates($username, $list = true) {
        $user = $this->find($username);
        $subordinates = array();

        if (isset($user [0] ['directreports']) && $user [0] ['directreports'] ['count'] > 0) {

            for ($i = 0; $i < $user [0] ['directreports'] ['count']; $i ++) {

                $userIdType = strtolower($this->userIdType);
                $userAccountControl = strtolower($this->userAccountControl);
                $subordinate = $this->findBy($user [0] ['directreports'] [$i], $this->distinguishedName);

                if (isset($subordinate[0][$userIdType][0]) && isset($subordinate[0][$userAccountControl][0]) && $subordinate[0][$userAccountControl][0] != self::USUARIO_INATIVO && $username != $subordinate[0][$userIdType][0]) {
                    $subordinates [$subordinate [0] [$userIdType] [0]] = $this->getSubordinates($subordinate [0] [$userIdType] [0], false);
                }
            }
        }

        return $list ? $this->listSubordinates($subordinates) : $subordinates;
    }

    /**
     *
     * @param unknown $username
     * @return boolean
     */
    public function isManager($username) {
        return (count($this->getSubordinates($username))) > 0;
    }

    /**
     *
     * @param unknown $subordinates
     * @return multitype:
     */
    public function listSubordinates($subordinates) {
        $retorno = array();
        $subordinates = is_array($subordinates) ? $subordinates : array();

        foreach ($subordinates as $key => $value) {
            if (!empty($value)) {
                array_push($retorno, $key);
                $retorno = array_merge($retorno, $this->listSubordinates($value));
            } else {
                array_push($retorno, $key);
            }
        }
        sort($retorno);
        return $retorno;
    }

    /**
     *
     * @param unknown $value
     * @param unknown $field
     * @return multitype:
     */
    public function findBy($value, $field) {
        $cacheKey = "{$field}={$value}";

        if (!$entries = $this->getFromCache($cacheKey)) {
            $r = @ldap_search($this->ds, $this->baseDn, $field . '=' . $value, $this->atributosValidos);

            if ($r) {
                ldap_sort($this->ds, $r, 'sn');
                $entries = ldap_get_entries($this->ds, $r);
                $this->writeCache($cacheKey, $entries);
            }
        }

        return $entries;
    }

    /**
     */
    public function usuariosAtivos() {
        
    }

    /**
     *
     * @param array $info
     */
    public function obterEntidade($info) {
        $entidade = null;

        if (isset($info [0] ['memberof'])) {
            for ($i = 0; $i < $info [0] ['memberof'] ['count']; $i ++) {
                if (preg_match('/(CN=(.*),)(OU=GruposCorreio)/', $info [0] ['memberof'] [$i], $matches)) {
                    if (isset($this->entidades [$matches [2]])) {
                        $entidade = $this->entidades [$matches [2]];
                        break;
                    }
                }
            }
        }

        if (!$entidade) {
            if (preg_match('/(OU=APP_MARANHAO)/', $info [0] ['distinguishedname'] [0], $matches)) {
                $entidade = $this->entidades ['filialma'];
            } else {
                $entidade = $this->entidades ['Matriz'];
            }
        }

        return $entidade;
    }

    /**
     */
    public function __clone() {
        trigger_error('Não se pode clonar essa classe', E_USER_ERROR);
    }

    /**
     * Obtem um resultado do cache na sessao
     *
     * @param string $key
     * @return string
     */
    private function getFromCache($key) {
        $session = $this->getCacheContainer();
        return ($session && $session->check($key)) ? $session->read($key) : null;
    }

    /**
     * Grava um resultado no cache da sessao
     *
     * @param string $key
     * @param string $value
     * @return boolean
     */
    private function writeCache($key, $value) {
        $session = $this->getCacheContainer();
        return ($session) ? $session->write($key, $value) : null;
    }

    private function getCacheContainer() {
        return (class_exists('SessionComponent')) ? new SessionComponent() : null;
    }

    /**
     *
     * @param string $setor
     * @return array
     */
    public function findUsersBySetor($setor, $somenteLogin = false, $baseDn = '') {
        $cacheKey = "department={$setor}";
        $baseDn = (empty($baseDn)) ? $this->baseDn : $baseDn;
        $usuarios = array();

        if (!$entries = $this->getFromCache($cacheKey)) {
            $r = @ldap_search($this->ds, $baseDn, $cacheKey, $this->atributosValidos);

            if ($r) {
                ldap_sort($this->ds, $r, 'sn');
                $entries = ldap_get_entries($this->ds, $r);
                $this->writeCache($cacheKey, $entries);
            }
        }

        if ($entries) {
            for ($i = 0; $i < $entries['count']; $i++) {
                $entries[$i]['manager'][0] = (isset($entries[$i]['manager'][0]) ? $entries[$i]['manager'][0] : '');
                $entries[$i]['userprincipalname'][0] = (isset($entries[$i]['userprincipalname'][0]) ? $entries[$i]['userprincipalname'][0] : '');
                $entries[$i]['department'][0] = (isset($entries[$i]['department'][0]) ? $entries[$i]['department'][0] : '');
                $entries[$i]['mail'][0] = (isset($entries[$i]['mail'][0]) ? $entries[$i]['mail'][0] : $entries[$i]['userprincipalname'][0]);
                $entidade = $this->obterEntidade($entries);

                if ($somenteLogin) {
                    $usuarios[$entries[$i]['samaccountname'][0]] = $entries[$i]['samaccountname'][0];
                } else {
                    $usuarios[] = array(
                        'nome' => $entries[$i]['cn'][0],
                        'email' => $entries[$i]['mail'][0],
                        'usuario' => $entries[$i]['samaccountname'][0],
                        'departamento' => $entries[$i]['department'][0],
                        'manager' => $entries[$i]['manager'][0],
                        'entidade' => $entidade
                    );
                }
            }
        }

        return $usuarios;
    }
}
