<?php

/*
 * Classe que provê métotdos comuns usados em vários models
 */

/**
 * Description of TableAbstract
 *
 * @author davisousa
 */
abstract class APP_TableAbstract extends Zend_Db_Table_Abstract{
    
    public function getTotalRegistros($where){
        
        $sql = 'SELECT count(1) as total FROM '.($this->_schema ? "{$this->_schema}." : "").$this->_name.' '.$this->montWhere($where);
       
         $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        
        
        return $dbAdapter->query($sql)->fetchAll();
    }
    
    public function listaRegistros($where,$order,$rpp,$inicio){
        
        $sql = "SELECT * FROM ".($this->_schema ? "{$this->_schema}." : "")."{$this->_name} ".$this->montWhere($where)." ORDER BY {$order} LIMIT {$rpp} OFFSET {$inicio}";
       
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        
        
        return $dbAdapter->query($sql)->fetchAll();
    }
    
    
    protected function montWhere($where){
        
        if(!isset($where)){
            return $where;
        }

        if(is_string($where)){
                $where1=$where;
        }else if(is_array($where)){
                $where1=implode(' AND ', $where);
        }

        if(is_string($where)){
            
            if($where == ''                    
            || substr_count($where, 'WHERE')>0 
            ){
                $where2 = $where;
              }else{
                $where2= 'WHERE '.$where1;
              }
        }else{
            $where2= 'WHERE '.$where1;
        }
        

         return (trim($where2) == 'WHERE' ? '' : $where2);
    }
    
    public function getPk(){
        
        $camposDb = new Zend_Session_Namespace('appi_db_cache_'.$this->_name);
        
        if(!empty($camposDb->pk)){
            return $camposDb->pk;
        }
        
        $sql = "SELECT
                c.table_name,c.column_name, c.data_type
                FROM
                information_schema.table_constraints tc 
                JOIN information_schema.constraint_column_usage AS ccu USING (constraint_schema, constraint_name) 
                JOIN information_schema.columns AS c ON c.table_schema = tc.constraint_schema AND tc.table_name = c.table_name AND ccu.column_name = c.column_name
                where constraint_type = 'PRIMARY KEY' and tc.table_name = '{$this->_name}';";
       
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        
        
         return $dbAdapter->query($sql)->fetchAll();
        
    }
    
    
    /**
     *Criado para ser sobrescrito na subclasse
     *Valida regras de negócio
     * Retorna um array com o código 0 se não houver erro, ou 1 se houver erro 
     * e em msg retorna a mensagem de sucesso ou erro.
     * @param mixed variáveis vindas pelo na requisição.
     * @return array Exemplo: array("cod"=>1,"msg"=>"Mensagem");
     */
    public function validate($params,$debug = false){
        
        return array("cod"=>0,"msg"=>"OK");
        
    }
    
    public function getName(){
        
        return $this->_name;
        
    }
    
    public function getSchema($comPonto = false){
        
        return ($this->_schema != '' ? $this->_schema.($comPonto ? "." : "") : '');
        
    }
    
    public function getSchemaTableName(){
        
        return $this->getSchema(true).$this->getName();
    }
    
    private function cleanCache($nameSpace,$indice = null,$arquivo = "file_cache.txt", $pasta = "../data/cache"){
        
        if(!is_dir("../data")){
            mkdir("../data");
        }
        
        if(!is_dir("../data/cache")){
            mkdir("../data/cache");
        }
        
        $caminhoArquivo = "{$pasta}/{$arquivo}";
        $conteudoArquivo = $this->getConteudoArquivo($caminhoArquivo);
        $conteudoArray = unserialize($conteudoArquivo);
        
        if(!empty($conteudoArray)){
            foreach ($conteudoArray as $nSpace => $conteudo) {
                if(substr_count($nSpace, $nameSpace)){
                    if($indice != null){
                        unset($conteudoArray[$nSpace][$indice]);
                    }

                    if($indice == null){
                        unset($conteudoArray[$nSpace]);
                    }

                }
            }
        }
        
        
        $fp = fopen($caminhoArquivo, "w");
        fwrite($fp, serialize($conteudoArray));
        fclose($fp);
        
    }
    
    public function cleanCacheDb(){
        $this->cleanCache("appi_db_cache_");
    }
    
    public function cleanPermissionsFromCache($idUsuario){
        $this->cleanCache("permissoes_usuario",$idUsuario);
    }
    
    public function getPermissionsFromCache($idUsuario){
        return $this->getFromCache("permissoes_usuario", $idUsuario);
    }
    
    public function storePermissionsInCache($idUsuario,$permissoes){
        $this->storeInCache("permissoes_usuario", $idUsuario, $permissoes);
    }
    
    private function storeInCache($nameSpace,$indice,$conteudo,$arquivo = "file_cache.txt", $pasta = "../data/cache"){
        
        if(!is_dir("../data")){
            mkdir("../data");
        }
        
        if(!is_dir("../data/cache")){
            mkdir("../data/cache");
        }
        
        $caminhoArquivo = "{$pasta}/{$arquivo}";
        $conteudoArquivo = $this->getConteudoArquivo($caminhoArquivo);
        $conteudoArray = unserialize($conteudoArquivo);
        $conteudoArray[$nameSpace][$indice] = $conteudo;
        
        $fp = fopen($caminhoArquivo, "w");
        fwrite($fp, serialize($conteudoArray));
        fclose($fp);
        
    }
    
    private function getFromCache($nameSpace,$indice,$arquivo = "file_cache.txt", $pasta = "../data/cache"){
        
        if(!is_dir("../data")){
            mkdir("../data");
        }
        
        if(!is_dir("../data/cache")){
            mkdir("../data/cache");
        }
        
        if(!is_file("{$pasta}/{$arquivo}")){
            return null;
        }
        
        $conteudoArquivo = $this->getConteudoArquivo("{$pasta}/{$arquivo}");
        $conteudoArray = unserialize($conteudoArquivo);
        return (isset($conteudoArray[$nameSpace][$indice]) ? $conteudoArray[$nameSpace][$indice] : null);
    }
    
    public function save($dados,$camposChaveComposta){
        
        $campoId = null;
        
        if(is_string($camposChaveComposta)){
            $campoId = $camposChaveComposta;
        }
        
        if($campoId == null){
             $dadosPk = $this->getPk();
             $campoId = $dadosPk[0]['column_name'];
        }
        
        
        $where = array();
        if(isset($dados[$campoId]) && !empty($dados[$campoId])){
            
            $value = (is_string($dados[$campoId]) ? "'{$dados[$campoId]}'" : $dados[$campoId]);
            $value = str_replace("''", "", $value);
            $where[]= "{$campoId} = {$value}";
            $r = $this->fetchRow($where);
            
            if(empty($r)){
                $this->insert($dados);
            }else{
                $this->update($dados,$where);
            }
        }
        
        if(is_array($camposChaveComposta)){
            foreach ($camposChaveComposta as $campo) {
                $campoPreenchido = (isset($dados[$campo]) && !empty($dados[$campo]));
                if($campoPreenchido){
                    $value = (is_string($dados[$campo]) ? "'{$dados[$campo]}'" : $dados[$campo]);
                    $where[]= "{$campo} = {$value}";
                }
            }
            
            if(!empty($where)){
                $r = $this->fetchRow($where);
                
                if(empty($r)){
                    $this->insert($dados);
                    $r = $this->fetchRow($where);
                }else{
                    $this->update($dados,$where);
                }
                
            }
            
        }
        
        if(is_a($r, "Zend_Db_Table_Rowset")){
            $r = $r->toArray();
        }
        
        $dadosPk = $this->getPk();
        $fieldPk = $dadosPk[0]['column_name'];
        
        return (isset($r[$fieldPk]) ? $r[$fieldPk] : 0);
        
    }
    
    private function getConteudoArquivo($caminhoArquivo){
        
        if(!is_file($caminhoArquivo)){
            $fp = fopen($caminhoArquivo, "w");
            fclose($fp);
        }
        
        $arrayLinhas = file($caminhoArquivo);
        
        $conteudoArquivo = "";
        foreach ($arrayLinhas as $linha) {
            $conteudoArquivo.=$linha;
        }
        
        return $conteudoArquivo;
    }

    
    public function getCampos(){
        
        $camposDb = new Zend_Session_Namespace('appi_db_cache_'.$this->_name);
            
        if(!empty($camposDb->campos)){
            return $camposDb->campos;
        }
        
        $schema = (!empty($this->_schema) ? $this->_schema : "public");
        
         $sql = "SELECT *
                FROM information_schema.columns
                WHERE table_schema = '{$schema}'
                  AND table_name   = '{$this->_name}'";
                  
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        
        $statement = $dbAdapter->query($sql);
        
        $dadosPk = $this->getPk();
        
        while ($row = $statement->fetch()) {
            
            $idPk = ($dadosPk[0]['column_name'] == $row['column_name']);
            
            $dados[$row['column_name']] = array('pk'=>$idPk,
                                                'column_name'=>$row['column_name'],
                                                'table_catalog'=>$row['table_catalog'],
                                                'table_schema'=>$row['table_schema'],
                                                'table_name'=>$row['table_name'],
                                                'data_type'=>$row['data_type'],
                                                'is_nullable'=>$row['is_nullable'],
                                                'is_identity'=>$row['is_identity'],
                                                'is_generated'=>$row['is_generated'],
                                                'character_maximum_length'=>$row['character_maximum_length']
                );
        }
        
        $camposDb->campos = $dados;
       
        return $dados;
        
    }
    
    public function beginTransaction() {
    $this->getAdapter()->beginTransaction();
    }
    public function commit() {
        $this->getAdapter()->commit();
    }
    public function rollback() {
        $this->getAdapter()->rollback();
    }
    
    
}

