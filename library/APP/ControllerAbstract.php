
<?php

/*
 * Provê métodos a serem utilizados nos controladoras filhos
 */

/**
 * Description of APP_ControllerAbstract
 *
 * @author davisousa
 */
abstract class APP_ControllerAbstract extends Zend_Controller_Action{
    
    protected $model;
    
    protected $campos = array();
    
    protected $campoId;
    
    protected $campoDesc;
    
    protected $dados;
    
    protected $lastInsertId;
    
    protected $dadosAntes;
    
    protected $dadosDepois;
    
    protected function protege(){
        
        $auth = new Zend_Session_Namespace('auth');
        
        if(!$auth->usuario){
          $this->getResponse()->setRedirect('/auth');
        }
        
    }
    
    public function init(){
        
        //mada o nome do controller e action atual para ser usado em qualuqer view
        $params = $this->_getAllParams();
        
        $this->view->controller = $params['controller'];
        $this->view->action = (!empty($params['action']) ? $params['action'] : "index");
        $this->view->module = $params['module'];
        //se o model não estiver instanciado, instancia o model com o mesmo nome do controller
        if(empty($this->model)){
           
            if($params['module'] == 'default'){
                eval('$this->model = new Application_Model_'.ucwords(str_replace("-", "", $params['controller'])).'();');
            }else{
                eval('$this->model = new '. ucwords($params['module']).'_Model_'.ucwords(str_replace("-", "", $params['controller'])).'();');                
            }
            
            
        }
        
        //popula os nomes dos campos da tabela
        if(empty($this->campos)){
            
            $camposAll = $this->model->getCampos();
            
            foreach ($camposAll as $nomeCampo => $dadosCampo) {
                $campos[] = $nomeCampo;
                if($dadosCampo['pk']){
                    $this->campoId = $nomeCampo;
                }
                
                if($dadosCampo['data_type'] == 'character varying'){
                    $this->campoDesc = $nomeCampo;
                }
                
            }
            
            $this->campos = $campos;
        }
        
        $this->protege();
        
        return parent::init();
    }
    
    
    public function indexAction()
    {
        
        $params = $this->_getAllParams();
        $paramStr = '';
        $where = array();
        
        $usuario = new Application_Model_Usuario();
        
        if(!$usuario->isAllowed($this->view->controller, $this->view->action,$this->view->module)){
            throw new Exception("Sem permissão para acessar esse recurso.");
        }
        
        
        if(!isset($params['campo'])){
            $params['campo'] = '';
        }
        
        if(!isset($params['filtro'])){
            $params['filtro'] = '';
        }
        
        if(!empty($params['filtro']) && !empty($params['campo'])){
            
            if(is_numeric($params['filtro'])){
                $where[] = $this->model->getAdapter()->quoteInto("{$params['campo']} = ?", $params['filtro']);
            }else if(is_string($params['filtro'])){
                $where[] = $this->model->getAdapter()->quoteInto("retira_acentuacao({$params['campo']}) ILIKE retira_acentuacao(?)", "{$params['filtro']}%");
            }
            
            $paramStr = "/campo/{$params['campo']}/filtro/{$params['filtro']}";
            
        }
        
        $pageModule = ($params['module'] != 'default' ? "/{$params['module']}" : "");
        
        $paginacao = APP_Util::paginacao($params, $this->model, 10, "{$pageModule}/{$params['controller']}/{$params['action']}".$paramStr, $where,"{$this->campoId} DESC");
        
        $this->view->paginacao = $paginacao;
        $this->view->campo = $params['campo'];
        $this->view->filtro = $params['filtro'];
        $this->view->usuarioModel = $usuario;
        
    }
    
    public function formAction(){
        
        $this->_helper->layout->disableLayout();
        
        $params = $this->_getAllParams();
        
        if(!empty($params['id'])){
           $r = $this->model->fetchRow("{$this->campoId} = {$params['id']}");
           
           if(is_a($r, "Zend_Db_Table_Row")){
               $r = $r->toArray();
           }
           
           $this->view->r = $r;
          
        }
        
        $usuario = new Application_Model_Usuario();
        $this->view->usuarioModel = $usuario;
        
    }
    
    public function salvarAction(){
        
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
        
        try {
            
            $params = $this->_getAllParams();
            
            $dados = array();
            
            //organiza os dados do formulário
            foreach ($this->campos as $campo) {
                    if(!isset($dados[$campo])
                    && isset($params[$campo])        
                    && $campo != $this->campoId){
                        $dados[$campo] = $params[$campo];
                    }
            }
            
            $this->dados = $dados;
            
            $this->model->beginTransaction();
            
            $this->beforeSave();
           
            $result = $this->model->validate($params);
           
            if($result['cod'] > 0){
                throw new Exception($result["msg"]);
            }
            
            $usuario = new Application_Model_Usuario(); 
            
            if(!empty($params['id'])){
                
                if(!$usuario->isAllowed($this->view->controller,'alterar',$this->view->module)){
                    throw new Exception("Sem permissão para fazer essa operação.");
                }
                
                $this->dadosAntes = $this->model->fetchRow("{$this->campoId} = {$params['id']}");
                if(is_a($this->dadosAntes, 'Zend_Db_Table_Row')){
                    $this->dadosAntes = $this->dadosAntes->toArray();
                }
                $this->model->update($this->dados,"{$this->campoId} = {$params['id']}");
                
                $this->dadosDepois = $this->model->fetchRow("{$this->campoId} = {$params['id']}");
                if(is_a($this->dadosDepois, 'Zend_Db_Table_Row')){
                    $this->dadosDepois = $this->dadosDepois->toArray();
                }
                
            }else{
                if(!$usuario->isAllowed($this->view->controller,'inserir',$this->view->module)){
                    throw new Exception("Sem permissão para fazer essa operação.");
                }
                
                $auth = new Zend_Session_Namespace('auth');
                
                //preenche automaticamente o usuário que inseriu 
                if(in_array('id_usuario_inseriu', $this->campos) 
                && !empty($auth->id_usuario)){
                    $this->dados['id_usuario_inseriu'] = $auth->id_usuario;
                }
                $tableName = $this->model->getSchemaTableName();
               
                $this->model->insert($this->dados);
                
                $db = $this->model->getAdapter();
            
                $pk = $this->model->getPk();

                $this->lastInsertId = $db->lastInsertId($tableName, $pk[0]['column_name']);
                
                $this->dadosDepois = $this->model->fetchRow("{$this->campoId} = {$this->lastInsertId}");
                if(is_a($this->dadosDepois, 'Zend_Db_Table_Row')){
                    $this->dadosDepois = $this->dadosDepois->toArray();
                }
                
            }
            
            
            
            
            $this->afterSave();
            
            $this->gravarLog();
            
            $this->model->commit();
            
            $json = array(
                        "msg"=>array("cod"=>0,
                                     "txt"=>"Dados salvos com sucesso.")
                            );
            
            
        } catch (Exception $e) {
            
            $this->model->rollback();
            
            $json = array(
                        "msg"=>array("cod"=>1,
                                     "txt"=>$e->getMessage()),
                                     "stacktrace"=>$e->getTraceAsString()   
                            );
        }
        $_POST['json'] = $json;
        echo $this->view->json($json);
        
    }
    
    private function gravarLog(){
        
        $params = $this->_getAllParams();
        
        $id = (isset($params['id']) && $params['id'] > 0 ? $params['id'] : $this->lastInsertId);
        
        $tablename = $this->model->getSchemaTableName();
        
        $log = new Application_Model_Log();
        
        $dadosAntes = (isset($params['id']) && $params['id'] > 0 ? $this->dadosAntes : null);
        
        $log->gravarLog($tablename,$id, $this->dadosDepois, $dadosAntes);
        
    }
    
    
    /**
     *Criado para ser sobrescrito na subclasse
     *      */
    public function afterSave(){
        
    }
    
    
    /**
     *Criado para ser sobrescrito na subclasse
     *      */
    public function beforeSave(){
        
    }
    
   
    
    
    public function excluirAction(){
        
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
        
        try {
            
            $params = $this->_getAllParams();
            
            $this->model->delete("{$this->campoId} = {$params['id']}");
            
             $json = array(
                        "msg"=>array("cod"=>0,
                                     "txt"=>"Registro excluído com sucesso.")
                            );
            
            
        } catch (Exception $e) {
             
            $json = array(
                        "msg"=>array("cod"=>1,
                                     "txt"=>$e->getMessage())
                            );
             
        }
        
        header('Content-Type: application/json');
        
        echo json_encode($json);
            
    }
    
}
