<?php

/**
 * classe com métodos estaticos ulteis
 * @author Davi Felipe <davifelipe@hotmail.com>
 * @version 1.2 - ultima alteracao: 24/06/2010
 */
class APP_Util{
    /**
     * método para converter data ou data e hora do formato (português brasil)pt-BR para (Ingles)en-EN
     * @param String $data Data no formato pt-BR
     * @return Retorna data no formato en_EN
     */
    public static function dateFormatToEn($data){

           //se já for formato EN retorna ele mesmo
           if(self::tipoDateFormat($data)){
                return $data;
           }else if(!is_string($data)){//se não for string retorna nulo
                return null;
           }

            $arrayData = explode('/', $data);

            //remove a hora do ano
            $arrayData[2] = substr($arrayData[2], 0, 4);

            //retira os espaços da data passada
            $data = trim($data);

            //se a data passado for string vazia, zera a data
            if($data == ''){
                $arrayData[0]='00';
                $arrayData[1]='00';
                $arrayData[2]='0000';
            }

            //se tiver hora, armezena,
            $hora = substr($data, 10);

            return $arrayData[2]."-".$arrayData[1]."-".$arrayData[0].$hora;


    }
    
    public static function getWeekOfYear($ddate){
        
        $ddate = self::dateFormatToEn($ddate);
        
        $duedt = explode("-",$ddate);
        $date = mktime(0, 0, 0, $duedt[1], $duedt[2],$duedt[0]);
        $week = (int)date('W', $date);
        
        return $week;
    }

    /**
     * testa formato da data
     * @param String data a ser testada
     * @return int 1 para format EN ou 0 para formato Br retorna null para
     * formato inválido
     */
     private static function tipoDateFormat($date){
         if(substr_count($date, '-')>0
         && !substr_count($date, '/')>0
         ){
              //indica EN
              return 1;
         }else if(!substr_count($date, '-')>0
               && substr_count($date, '/')>0
         ){
             //indica BR
             return 0;
         }else{
             return null;
         }

     }

    /**
     * corta uma string
     * @param int $intLength Tamano da strig depois que for cortada
     * @param Strin $strText String a ser cortada
     * @return Strin String cortada
     */
    public static function truncateString($string,$length,$end='...',$encoding=null){
            if(!$encoding) $encoding = 'UTF-8';
            $string = trim($string);
            $len = mb_strlen($string,$encoding);
            if($len <= $length) return $string;
            else {
                $return = mb_substr($string,0,$length,$encoding);
                return (preg_match('/^(.*[^\s])\s+[^\s]*$/', $return, $matches) ? $matches[1] : $return).$end;
            }
    }




    /**
     * método para converter data ou data e hora do formato (Ingles)en-EN para (português brasil)pt-BR
     * @param String $data Data no formato en-EN
     * @return Retorna data no formato pt_BR
     */
    public static function dateFormatToPt($data){

          //se já for formato PT retorna ele mesmo
           if(!self::tipoDateFormat($data)){
                return $data;
           }else if(!is_string($data)){//se não for string retorna nulo
                return null;
           }

            $arrayData = explode('-', $data);

            //remove a hora do array 2
            $arrayData[2] = substr($arrayData[2], 0, 2);

            //retira os espaços da data passada
            $data = trim($data);

            //se a data passado for string vazia, zera a data
            if($data == ''){
                $arrayData[0]='0000';
                $arrayData[1]='00';
                $arrayData[2]='00';
            }
            //se tiver hora, armezena,
            $hora = substr($data, 10, 11);


           return $arrayData[2]."/".$arrayData[1]."/".$arrayData[0].$hora;


    }

    /**
     * m�todo para converter numero float EN para PT
     * substituindo (.) por (,)
     * @param mixed numero ou string com (.)
     * @return retorna numero com (,) ao inves de (.)
     */
     public static function floatToPT($float){

         if(substr_count($float, ',')>0){
            return $float;
         }

         $result = number_format($float,2,",",".");
         return $result;

     }

     /**
     * m�todo para converter numero float PT para EN
     * substituindo (,) por (.)
     * @param mixed numero ou string com (,)
     * @return retorna numero com (.) ao inves de (,)
     */
     public static function floatToEN($float){

         if(substr_count($float, ',') == 0){
             return $float;
         }

         //remove o (.) dos milhares
         $float = str_replace('.', '', $float);

         //subustitui (,) por .
         $float = str_replace(',', '.', $float);


         return $float;

     }

     /**
      * coloca um determinado caractere no final da string caso ainda n�o tenha
      * @param String $char Caractere a ser colocado no final caso n�o tenha
      * @param String $string String a ser tratada
      * @return String Retorna a string tratada
      */
      public static function putEndIfNot($char, $string){

            $ref= strlen($string)-1;

            $ultimo=substr($string, $ref, 1);

            if($ultimo != $char){
                    $string=$string.$char;
            }
            //retorna a string tratada
            return $string;
      }

       /**
        * @return boolean true para email válio ou false se for inválido
        */
        public static function validarEmail($email){
        $mail_correcto = 0;
        //verifico umas coisas
        if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@")){
              if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," "))) {
                 //vejo se tem caracter .
                 if (substr_count($email,".")>= 1){
                    //obtenho a termina��o do dominio
                    $term_dom = substr(strrchr ($email, '.'),1);
                    //verifico que a termina��o do dominio seja correcta
                 if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){
                    //verifico que o de antes do dominio seja correcto
                    $antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1);
                    $caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1);
                    if ($caracter_ult != "@" && $caracter_ult != "."){
                       $mail_correcto = 1;
                    }
                 }
              }
           }
        }

        if ($mail_correcto)
           return 1;
        else
           return 0;
        }

   private static function isMaskedCnpj($cnpj){
       if(substr_count($cnpj,".")>0
       && substr_count($cnpj,"/")>0
        ){
            return true;
        }else{
            return false;
        }
   }

   private static function isMaskedCpf($cpf){
       if(substr_count($cpf,".")>0
       && substr_count($cpf,"-")>0
       && substr_count($cpf,"/") == 0
        ){
            return true;
        }else{
            return false;
        }
   }

   /**
    * tipo de inscri��o testa se a inscri��o � cnpj ou cpf
    * @param inscri��o CPF ou CNPJ mascarado ou desmascarado
    * @return int 1 para CNPJ ou 0 para CPF
    */
   public static function tipoInscricao($inscricao){

            //quantidade de caracteres
            $tInscricao = strlen($inscricao);


        //se a inscri��o estiver mascarada, se fo um cnpj mascarado ou cpf mascarado
            if(self::isMaskedCnpj($inscricao)
            || self::isMaskedCpf($inscricao)
            ){
                $mascarado = true;

            }else{
                $mascarado = false;

            }



        //se for == 11
            //e tiver desmascarado
            //ent�o � cpf desmascarado
            if($tInscricao == 11 && !$mascarado){

                //indica cpf
                return false;

             }else if($tInscricao ==14 && !$mascarado){

                 //indica cnpj
                 return true;

             }else if($mascarado){//se esvier mascarado
                 if(self::isMaskedCnpj($inscricao)){
                     //indica cnpj
                     return true;
                 }else{
                     //indica cpf
                      return false;
                 }
             }else{//n�o for incri��o v�lida, marca cnpj
                //indica cnpj
                 return true;
             }
   }

      /**
     * m�todo para mascarar CNPJ
     * @param $cnpjImp = n�mero do cnpj
     * @access private
     */

    private static function maskaraCnpj($cnpj){

        //se j� estiver maskarado retorna
        if(self::isMaskedCnpj($cnpj)){
            return $cnpj;
        }

		$cnpj0=substr($cnpj,0,2);
		$cnpj1=substr($cnpj,2,3);
		$cnpj2=substr($cnpj,5,3);
		$cnpj3=substr($cnpj,8,4);
		$cnpj4=substr($cnpj,12,2);
		$cnpjF=$cnpj0.'.'.$cnpj1.'.'.$cnpj2.'/'.$cnpj3.'-'.$cnpj4;

        return $cnpjF;

    }//fim do m�todo


    /**
     * m�todo para mascarar CPF
     * @param $cnpjImp = n�mero do CPF
     * @access private
     */

    private static function maskaraCPF($cpf){

        //se j� estiver mascarado, retorna
        if(self::isMaskedCpf($cpf)){
            return $cpf;
        }

		$cpf0=substr($cpf,0,3);
		$cpf1=substr($cpf,3,3);
		$cpf2=substr($cpf,6,3);
		$cpf3=substr($cpf,9,2);
		$cpfF=$cpf0.'.'.$cpf1.'.'.$cpf2.'-'.$cpf3;

        return $cpfF;

    }//fim do m�todo

    /**
     * mascara inscrição, já testa se é cpf ou cnpj
     * @param int inscrição desmascarada
     * @return String inscriçao mascarada. Se for passada uma inscrição já mascarada, retorna ela mesma
     */
    public static function maskaraInscricao($insc){
        //se for um cpf desmascarado
        if(!self::isMaskedCpf($insc)
        && !self::tipoInscricao($insc)//se for cpf
        ){

            $saida = self::maskaraCPF($insc);

            return $saida;
        }

        //se for um cnpj desmascarado
        if(!self::isMaskedCnpj($insc)
        && self::tipoInscricao($insc)//e for cnpj
        ){
            $saida = self::maskaraCnpj($insc);
            return $saida;
        }else{
            //se não for nem cpf nem cnpj desmascarado, retorna ele mesmo
            return $insc;
        }

    }

    /**
     * desmascara cnpj
     * @param String numero do CNPJ mascarado
     * @return int Cnpj desmascarado
     */
    private static function desmaskaraCnpj($cnpj){

        //se j� estiver desmascarado, retorna
        if(!self::isMaskedCnpj($cnpj)){
            return $cnpj;
        }
        //11.111.111/1111-80
        $cnpj1=substr($cnpj, 0, 2);
        $cnpj2=substr($cnpj, 3, 3);
        $cnpj3=substr($cnpj, 7, 3);
        $cnpj4=substr($cnpj, 11, 4);
        $cnpj5=substr($cnpj, 16, 2);

        $cnpj = $cnpj1.$cnpj2.$cnpj3.$cnpj4.$cnpj5;

        return $cnpj;
    }

    /**
     * desmascara CPF
     * @param String CPF mascarado
     * @return int cpf desmacarado
     */
    private static function desmaskaraCPF($cpf){

        //se j� estiver desmascarado, retorna
        if(!self::isMaskedCpf($cpf)){
            return $cpf;
        }

		$cpf0=substr($cpf,0,3);
    $cpf1=substr($cpf,4,3);
		$cpf2=substr($cpf,8,3);
		$cpf3=substr($cpf,12,2);
		$cpfF=$cpf0.$cpf1.$cpf2.$cpf3;

        return $cpfF;

    }//fim do m�todo


    /**
     * desmascara inscrição, já testa se é cpf ou cnpj
     * @param String inscrição mascarada
     * @return int inscrição desmascarada. Se for passada uma inscrição já desmascarada, retorna ela mesma
     */
    public static function desmaskaraInscricao($insc){
        //se for um cpf mascarado
        if(self::isMaskedCpf($insc)){

            $saida = self::desmaskaraCPF($insc);

            return $saida;
        }

        //se for um cnpj mascarado
        if(self::isMaskedCnpj($insc)){
            $saida = self::desmaskaraCnpj($insc);
            return $saida;
        }else{
            //se não for nem cpf nem cnpj mascarado, retorna ele mesmo
            return $insc;
        }

    }

     /**
     * m�todo para validar CNPJ
     * @param $cnpj = String: CNPJ com formata��o
     */

    public static function validaCNPJ($cnpj) {

      //se for menor que 18 mascara
    if (strlen($cnpj) < 18){
        $cnpj=self::maskaraCnpj($cnpj);
    }

     //se for direrente que 18 depois de mascarado
    //retorna falso
    if(strlen($cnpj) != 18){
        return 0;
    }

    $soma1 = ($cnpj[0] * 5) +

    ($cnpj[1] * 4) +
    ($cnpj[3] * 3) +
    ($cnpj[4] * 2) +
    ($cnpj[5] * 9) +
    ($cnpj[7] * 8) +
    ($cnpj[8] * 7) +
    ($cnpj[9] * 6) +
    ($cnpj[11] * 5) +
    ($cnpj[12] * 4) +
    ($cnpj[13] * 3) +
    ($cnpj[14] * 2);
    $resto = $soma1 % 11;
    $digito1 = $resto < 2 ? 0 : 11 - $resto;
    $soma2 = ($cnpj[0] * 6) +

    ($cnpj[1] * 5) +
    ($cnpj[3] * 4) +
    ($cnpj[4] * 3) +
    ($cnpj[5] * 2) +
    ($cnpj[7] * 9) +
    ($cnpj[8] * 8) +
    ($cnpj[9] * 7) +
    ($cnpj[11] * 6) +
    ($cnpj[12] * 5) +
    ($cnpj[13] * 4) +
    ($cnpj[14] * 3) +
    ($cnpj[16] * 2);
    $resto = $soma2 % 11;
    $digito2 = $resto < 2 ? 0 : 11 - $resto;
    return (($cnpj[16] == $digito1) && ($cnpj[17] == $digito2));
    }



    /**
     * fun��o para validar CPF
     * @param $cpf = numero do cpf mascarado ou desmascarado
     */
    public static function validaCPF($cpf){// Verifiva se o n�mero digitado cont�m todos os digitos


        //se o cpf for maior que 11 desmascara
        if(strlen($cpf) > 11){
            $cpf=self::desmaskaraCPF($cpf);
        }



        $cpf = str_pad(ereg_replace('[^0-9]', '', $cpf), 11, '0', STR_PAD_LEFT);

        // Verifica se nenhuma das sequ�ncias abaixo foi digitada, caso seja, retorna falso
        if (strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999'){
        return false;
        }else{   // Calcula os n�meros para verificar se o CPF � verdadeiro
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }

                $d = ((10 * $d) % 11) % 10;

                if ($cpf{$c} != $d) {
                    return false;
                }
            }

            return true;
        }
    }

    /**
     * valida inscri��o tanto CPF como CNPJ, mascarado ou n�o
     * @param String|int $inscri��o numero de inscri��o
     * @return boolean true para inscri��o v�lida ou false para inscri��o inv�lida
     */
     public static function validaInscricao($inscricao){
           //se for CNPJ
           if(self::tipoInscricao($inscricao)){
               $saida= self::validaCNPJ($inscricao);
               return $saida;
           }else{//se for CPF
               $saida = self::validaCPF($inscricao);
               return $saida;
           }
     }

    /**
     * método para gerar miniatura de imagems
     * @param String $imagem caminho e nome do arquivo de imagem ex: 'pasta/imagem.jpg'
     * @param String $output Caminho e nome do arquivo de miniatura gerado
     * ex: 'nova_pasta/miniatura.jpg'
     * @param int $new_width largura em pixels da imagem de miniatura gerada
     * @param int[opcional] $new_height altura em pixels da imagem de miniatura gerada
     * @param boolean[opcional] $esticar Default: true. Se estiver setado altura e for true, estica . Se setar altura e for false, corta a imagem na altura selecionada
     * @return void
     */
    public static function geraThumbJPG($imagem, $output, $new_width, $new_height = '',$esticar =true){

              //aumenta o limite de mamoria para não dar problemas quando for gerar miniatura
              ini_set("memory_limit","35M");

               //armazena a imagem selecionada
               $source = imagecreatefromstring(file_get_contents($imagem));

               //pega a altura a largura da imagem
                list($width, $height) = getimagesize($imagem);

                if ($width>$new_width){

                    if($new_height == ''){//se não estiver setado altura

                         $new_height = ($new_width/$width) * $height;
                    }

                    $thumb = imagecreatetruecolor($new_width, $new_height);

                    if($esticar){//se for esticar
                        imagecopyresampled($thumb, $source, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                    }else{//se não..

                        //gera uma miniatura sem esticar
                        $new_height1 = ($new_width/$width) * $height;

                        imagecopyresampled($thumb, $source, 0, 0, 0, 0, $new_width, $new_height1, $width, $height);

                        imagejpeg($thumb, $output, 100);

                        //depois corta a miniatura gerada na altura setada
                        $source = imagecreatefromstring(file_get_contents($imagem));

                        imagecopyresampled($thumb, $source, 0, 0, 0, 0, $new_width, $new_height, $new_width, $new_height);
                    }


                    imagejpeg($thumb, $output, 100);
                }
                else
                {
                    copy($imagem, $output);
                }

                unset($imagem);
                unset($output);
                unset($source);
                unset($new_height);
    }

    /**
     * método para gerar miniatura de imagems
     * @param String $imagem caminho e nome do arquivo de imagem ex: 'pasta/imagem.jpg'
     * @param String $output Caminho e nome do arquivo de miniatura gerado
     * ex: 'nova_pasta/miniatura.jpg'
     * @param int $new_width largura em pixels da imagem de miniatura gerada
     * @param int[opcional] $new_height altura em pixels da imagem de miniatura gerada
     * @param boolean[opcional] $esticar Default: true. Se estiver setado altura e for true, estica . Se setar altura e for false, corta a imagem na altura selecionada
     * @return void
     */
    public static function geraThumb($imagem, $output, $new_width, $new_height = '',$esticar =true){

              //aumenta o limite de mamoria para não dar problemas quando for gerar miniatura
              ini_set("memory_limit","100M");

              $isPng = (substr_count($imagem, '.png')>0);

              //armazena a imagem selecionada
               if ( $isPng ){
                   $source = imagecreatefrompng($imagem);
               }else{
                   $source = imagecreatefromjpeg($imagem);
               }

               $isTrueColor = imageistruecolor($source);

               //pega a altura a largura da imagem
                list($width, $height) = getimagesize($imagem);

                if ($width>$new_width){

                    if($new_height == ''){//se não estiver setado altura

                         $new_height = ($new_width/$width) * $height;
                    }

                    if ( $isTrueColor){

                           $thumb = imagecreatetruecolor($new_width, $new_height);
                            imagealphablending($thumb, false);
                            imagesavealpha  ( $thumb  , true );
                    }else{
                        $thumb  = imagecreate( $new_width, $new_height );
                        imagealphablending( $thumb, false );
                        $transparent = imagecolorallocatealpha( $thumb, 0, 0, 0, 127 );
                        imagefill( $thumb, 0, 0, $transparent );
                        imagesavealpha( $thumb,true );
                        imagealphablending( $thumb, true );
                    }


                    if($esticar){//se for esticar

                        imagecopyresampled($thumb, $source, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                    }else{//se não..

                        //gera uma miniatura sem esticar
                        $new_height1 = ($new_width/$width) * $height;

                        //imagecopyresampled($thumb, $source, 0, 0, 0, 0, $new_width, $new_height1, $width, $height);

                        imagepng($thumb, $output, 100);

                        //depois corta a miniatura gerada na altura setada
                        $source = imagecreatefromstring(file_get_contents($imagem));

                        imagecopyresampled($thumb, $source, 0, 0, 0, 0, $new_width, $new_height, $new_width, $new_height);
                    }

                    //header("Content-Type: image/png");
                    //imagepng($thumb);die();
                    //imagejpeg($thumb, $output, 100);
                    imagepng($thumb,$output);
                }
                else
                {
                    copy($imagem, $output);
                }

                imagedestroy($source);

                unset($imagem);
                unset($output);
                unset($source);
                unset($new_height);
    }

    /**
     * @param int $uploadErro $_FILES['element']['error']
     * @return String erro do codigo do erro ou false se o código for inválido
     */
    public static function getErroUpload($uploadErro){

        switch($uploadErro)
                {

                    case '1':
                        $error= 'O arquivo exedeu o tamanho máximo : 2MB';
                        break;
                    case '2':
                        $error= 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                        break;
                    case '3':
                        $error= 'The uploaded file was only partially uploaded';
                        break;
                    case '4':
                        $error= 'Arquivo não selecionado!';
                        break;

                    case '6':
                        $error= 'Missing a temporary folder';
                        break;
                    case '7':
                        $error= 'Failed to write file to disk';
                        break;
                    case '8':
                        $error= 'File upload stopped by extension';
                        break;
                    case '999':
                    default:
                        $error= false;
                }

                        return $error;
    }

    /**
     * método para gerar numeros sequenciais armazenados em um arquivo de texto
     * @param String $arquivoContador Caminho e nome do arquivo de texto que vai armazenar
     * os números sequenciais ex: 'pasta/contador.txt'
     * @return int número sequencial gerado depois do incremento
     */
    public static function geraNumSequencial($arquivoContador){

            //testa se o arquivo existe
            if(!file_exists($arquivoContador)){
                 die('O arquivo '.$arquivoContador.' não existe!');
             }

          try {

              //abre o arquivo como append
			  $fp = fopen($arquivoContador,'a+');
			  //pega o conteudo do arquivo
			  $data = fgets($fp, 4096);
			  //incrementa o conteudo antigo
			  $data++;
			  //fecha o arquivo
			  fclose($fp);
			  //abre o arquivo novamente mas agora como sobrescrita
			  $fpw=fopen($arquivoContador,'w');
			  //escrevo o conteudo novo no arquivo
			  fwrite($fpw, $data);
			  //finalmente fecha o arquivo
			  fclose($fpw);

          } catch (Exception $e) {

              echo 'Erro interno em Util::gerar número sequencial: '.$e;
          }

			  //retorna o valor encrementado
			  return $data;

    }

    /**
     * método para o recuperar o conteudo de um arquivo de texto
     * @param String $arquivoContador Caminho e nome do arquivo de texto que vai armazenar
     * os números sequenciais ex: 'pasta/contador.txt'
     * @return mixed conteudo do arquivo
     */
    public static function getConteudo($arquivo){

        //testa se o arquivo existe
        if(!file_exists($arquivo)){
                 die('O arquivo '.$arquivo.' não existe!');
        }

        try {

             //abre o arquivo como append
            $fp = fopen($arquivo,'a+');
            //pega o conteudo do arquivo
            $data = fgets($fp, 4096);
            //fecha o arquivo
            fclose($fp);

        } catch (Exception $e) {

             echo 'Erro interno em Util::get conterudo : '.$e;
        }

            return $data;
    }

    /**
     * método para testar extenssão de um determinado arquivo
     * @param Array $extValidas array contendo extenssões válidas ex: array('jpg','png');
     * @param String $arquivo nome do arquivo a ser testado ex: 'arquivo.jpg'
     * @return boolean true para arquivo ok ou false para extensão inválida
     */
    public static function testaExtensao(array $extValidas,$arquivo){

             $ok = null;

             //resgata a extensão do arquivo
             $ext = strchr ("$arquivo",'.');

             //coloca . antes do nome da extensão se não tiver
             foreach ($extValidas as $key => $value) {
                 if(!substr($value, 0, 1 == '.')){
                    $value='.'.$value;
                }

                if($ext == $value){
                    $ok = 1;
                }
            }


           if (!$ok){
                //se a extensão não estiver em extenssões válidas
                return false;
            }else{
                return true;
            }
    }

    /**
     * formata número ex: 123 $loop 10 retorna 0000000123
     * @param Mixed $numero Numero a ser formatado, pode ser float com virgula
     * @param int $loop indica quantos caracteres vai ter o numero de retorno
     * @param Mixed $insert Caractere a ser inserido até completar o tamanho
     * @param Strin[opcional] $tipo titpo de numero
     */
    public static function formata_numero($numero,$loop,$insert,$tipo = "geral") {


	if ($tipo == "geral") {
		$numero = str_replace(",","",$numero);
		while(strlen($numero)<$loop){
			$numero = $insert . $numero;
		}
	}
	if ($tipo == "valor") {
		/*
		retira as virgulas
		formata o numero
		preenche com zeros
		*/
		$numero = str_replace(",","",$numero);
		while(strlen($numero)<$loop){
			$numero = $insert . $numero;
		}
	}
	if ($tipo == "convenio") {
		while(strlen($numero)<$loop){
			$numero = $numero . $insert;
		}
	}
	return $numero;
}

 /**
     * escreve o conteudo em algum arquivo de texto passado
     * @param String $ponteiro Caminho do arquivo
     * @param Mixed $conteudo Conte�do que vai ser escrito no arquivo
     * @param String [opcional] $modo parametro modo. ex: 'a'
     * @return boolean False se n�o coseguir escrever, ou true se der certo
     */
    public static function escrever($ponteiro,$conteudo,$modo = "a"){

                try {

                    $fp = fopen($ponteiro, $modo);
                    $escreve = fwrite($fp, $conteudo);

                    if($escreve){
                        return true;
                    }

                } catch (Exception $e) {

                    fclose($fp);
                    return false;
                }
                   fclose($fp);

    }

    /**
     * soma datas
     */
    public static function somarData($data, $dias, $meses, $ano){

               //se não for formato PT converte
           if(self::tipoDateFormat($data)){
               $data = self::dateFormatToPt($data);
           }

               $data = explode('/', $data);
               $newData = date('d/m/Y', mktime(0, 0, 0, $data[1] + $meses,
               $data[0] + $dias, $data[2] + $ano));
               return $newData;
    }

    /**
     * @param SMVC_EntityAbstract $entidade entidade
     * @param String $campoId Nome do campo id
     * @param String $campoNome Nome do campo que vai aparecer
     * @param String|array[opcional] $where Clausula where para a consulta
     * @return String contendo codigogo html com options para popular uma combobox
     */
    public static function getOption(SMVC_EntityAbstract $entidade,$campoId,$campoNome,$where = null){

        $dados = $entidade->fetchAll($where);

        if(!is_array($dados)){
            $dados = array();
        }

        foreach ($dados as $key => $registro) {
            $optionSaida.='<option value="'.$registro[$campoId].'">'.$registro[$campoNome].'</option>
                            ';
        }
            return $optionSaida;
    }

    /**
     * remove parte da string depois de um determinado caractere
     * @param String $caractere caractere existente na string
     * @param String $string String a ser tratada
     * @return String string tratada
     */
    public static function removeDepoisDe($caractere,$string){
        
         if(substr_count($string, $caractere)>0){
             $pos = strpos($string, $caractere);

            $string = substr($string, 0, $pos);

            $string = str_replace($caractere.$seqi, '', $string);
         }

            return $string;
    }

     /**
     * coloca as linhas do arquivo em um array
     * @param String[opcional] $dir Diretorio de onde se encontra o arquivo
     * @param String $nome Nome do arquivo
     */
    public static function arquivoToArray($nomeArquivo,$pasta = null){

        if($pasta != '' && $pasta!= null){
            $pasta = $pasta.'/';
            $pasta = str_replace('//', '/', $pasta);
            $caminho = $pasta.$nomeArquivo;
        }else{
            $caminho = $nomeArquivo;
        }




        if(!file_exists($caminho)){

            echo("arquivo nao encontrado: ".$caminho);

           return false;
        }

        //abre o arquivo
        $ponteiro = fopen ($caminho, "r");

        //percorre as linhas do arquivo
        while (!feof ($ponteiro)) {
            //l� uma linha do arquivo
            $linha = fgets($ponteiro, 4096);

            if($linha != ''){
                $arrayArquivo[]=$linha;
            }


        }//fecha o while

        //fecha o ponteiro do arquivo
        fclose ($ponteiro);

            return $arrayArquivo;

    }

   

    /**
     * @param float $valorMenor
     * @param float $valorMaior
     * @return porcentagem entre dois valores
     */
    public static function getPorcentagem($valorMenor,$valorMaior){

        $valorMenor = self::floatToEN($valorMenor);
        $valorMaior = self::floatToEN($valorMaior);

            if($valorMenor>$valorMaior){
                $saida = false;
            }else{
                $saida = ($valorMenor*100)/$valorMaior;
                $saida = number_format($saida, 2);
            }

            return $saida;

    }

     /**
       *clacula digito verificador pelo modulo 11
       *@param int $codigo c�digo a ser claculado
       *@param int[opcional] $peso padr�o 2
       *@param int[opcional] $base base para calculo padr�o 9
       *@return int Digito verificador calculado
      */
     public static function modulo11($codigo, $peso=2, $base=9){

         //come�a com 2 e diminui a cada intera��o
         $linha2=2;

         $char=strlen($codigo)-1;

         $limite = $base+1;

         //a base tem que ser maior que o peso
         if($peso>$base){
             die('Erro interno em Util::modulo11: a base deve ser maior que o peso!');
         }

         for ($j = $char ; $j >-1  ; $j--) {

             //quando chega em $limite, volta a ser $peso
             if($linha2 == $limite){
                $linha2=$peso;
             }

             //pega caractere da linha 1
             $linha1=substr($codigo,$j,1);

             //armazena produto, linha 1 e linha2
             $produto=$linha1*$linha2;

             //acumula soma
             $soma=$soma+$produto;

             //decrementa a linha2
             $linha2++;

         }//fim do la�o


         //resto da divis�o da soma por 11
         $RestoDivisao=$soma%11;
         //subtraindo 11 pelo resto
         $sub=11-$RestoDivisao;
         //se o resultado for maior que 9, o digito � zero
         if($sub>9){
             $digitoCalculado=0;
         }else{
             $digitoCalculado=$sub;
         }

         //retorna digito calculado
        return $digitoCalculado;


     }//fim do m�todo

     /**
      * @param String $string a ser tratada
      * @param String $espaco caractere a ser substituido pelo espaço
      * @return String string tratada sem caracteres especiais
      */
     public static function removeCaracteresEspeciais($string, $espaco = null){

         if(!isset($espaco)){
             $espaco = '_';
         }

         $aSaida = array('á','à','ã','â','é','ê','í','ó','ô','õ','ú','ü','ç','Á','À','Ã','Â','É','Ê','Í','Ó','Ô','Õ','Ú','Ü','Ç',' ');

        $entrada = "aaaaeeiooouucAAAAEEIOOOUUC".$espaco;

        $tamEntar = strlen($entrada);

         for ($i = 0 ; $i < $tamEntar ; $i++) {

            $charEntrar = substr($entrada, $i, 1);
            $charSair = $aSaida[$i];
            if(substr_count($string, $charSair)>0){
                $string = str_replace($charSair, $charEntrar, $string);
            }

        }
        
        return $string;
         

     }

     /**
      * testa se uma string tem caracteres especiais
      */
     public static function temCaracteresEspeciais($string){

            if(!strlen($string)>0){
                return false;
            }

           $numeros = '0123456789';
           $min = "abcdefghijlmnopqrstuvxzky";
           $maiusc = strtoupper($min);
           
           $entrada = $numeros.$min.$maiusc.' ';

          $tamEntar = strlen($string);

         for ($i = 0 ; $i < $tamEntar ; $i++) {

            $charValido = substr($string, $i, 1);
           
            if(!substr_count($entrada, $charValido)>0){
               $especial = 1;
            }

        }
                if(isset($especial)){
                    return true;
                }else{
                    return false;
                }
     }

     /**
      * Converte caracteres esceciais para html
      * exemplo:  é para &eacute;
      * @param String $string a ser tratada
      * @param String $espaco caractere a ser substituido pelo espaço
      * @return String string tratada sem caracteres especiais
      */
     public static function converteEspecialCharsParaHtml($string){


         $aSaida = array('á','à','ã','â','é','ê','í','ó','ô','õ','ú','ü','ç','Á','À','Ã','Â','É','Ê','Í','Ó','Ô','Õ','Ú','Ü','Ç',' ','º','°','ª');

         $aEntrada = array('&aacute;','&agrave;','&atilde;','&acirc;','&eacute;','&ecirc;','&iacute;','&oacute;','&ocirc;','&otilde;','&uacute;','&uuml;','&ccedil;','&Aacute;','&Agrave;','&Atilde;','&Acirc;','&Eacute;','&Ecirc;','&Iacute;','&Oacute;','&Ocirc;','&Otilde;','&Uacute;','&Uuml;','&Ccedil;',' ','&ordm;','&ordm;','&ordf;');


        $tamEntar = strlen($entrada);

         foreach ($aEntrada as $key => $charEntrada) {

              $string = str_replace($aSaida[$key], $charEntrada, $string);

         }

        

        return $string;


     }

     /**
      * retorna o valor entre dois chars ex: 'type(int)' retorna (int)
      * @param String $str string a ser tratada
      * @param String $char
      */
     public static function getStringEntreChar($str,$char,$char2 = null,$removeChars = true,$inverso = false){

         if(substr_count($str, $char)<2 && $char2 == null){
                return $str;
         }

         $t = strlen($str);

         for ($i = 0; $i < $t; $i++) {
                $ch = substr($str, $i, 1);
                
                if($ch == $char){
                    $p = 1;
                }


                if($p == 1){
                    
                      $saida.= $ch;

                      if($char2 == $ch){

                          $p = 2;

                      }

                      if(substr_count($saida, $char)>1){
                            $p = 2;
                       }

                       if($p == 2) $key++;

                }

                    
             }

             if($removeChars){
                 $saida = str_replace($char, '', $saida);
                 $saida = str_replace($char2, '', $saida);
             }

             if($inverso){

                 
                 $str = str_replace($char, '', $str);
                 $str = str_replace($char2, '', $str);
                 $str = str_replace($saida, '', $str);

                 return $str;
             }

             return $saida;

     }

     /**
      * envia um email
      * @param String $emailDestinatario
      * @param String $DestinatarioNome
      * @param String $mensagem
      * @param String $subject
      * @param String $nomeRemetente
      * @param String $emailResposta
      */
     public static function sendGenerico($emailDestinatario, $emailRemetente,$DestinatarioNome,$mensagem,$subject = null, $nomeRemetente = null,$emailResposta = null){

                $mail = new LP_Mailer_PHPMailer();

                $mail->SetLanguage("br"); // Define o Idioma
                $mail->CharSet = "utf-8"; // Define a Codificação
                $mail->IsSMTP(); // Define que será enviado por SMTP
                $mail->Host = SMVC_Start::getSmtpHost(); // Servidor SMTP
                $mail->SMTPAuth = true; // Caso o servidor SMTP precise de autenticação
                $mail->Username = SMVC_Start::getSmtpUser(); // Usuário ou E-mail para autenticação no SMTP
                $mail->Password = SMVC_Start::getSmtpPass(); // Senha do E-mail
                $mail->IsHTML(true); // Enviar como HTML
                $mail->From = $emailRemetente; // Define o Remetente
                $mail->FromName = ($nomeRemetente ? $nomeRemetente : "envio automatico"); // Nome do Remetente

                $mail->AddAddress($emailDestinatario,
                                  $DestinatarioNome); // Email e Nome do destinatário

                if(SMVC_Start::getSmtpPort() >0){
                     $mail->Port=SMVC_Start::getSmtpPort();
                }
                $mail->Subject = ($subject ? $subject : "envio automatico"); // Define o Assunto


                $mail->Body = $mensagem;

                if(isset($emailResposta)){
                    $mail->AddReplyTo($emailResposta); // E-mail de Resposta
                }

                if(!$mail->Send()){

                       return false;

                }else{
                         return true;

                }

    }

    /**
     * retorna uma data em array
     * @param String $data data em qualquer formato
     * @return Array
     * $data['dia']
     * $data['mes']
     * $data['ano']
     */
    public static function dataToArray($data){

        $data = self::dateFormatToPt($data);

        $array = explode('/', $data);

        $saida['dia'] = $array[0];
        $saida['mes'] = $array[1];
        $saida['ano'] = $array[2];

        return $saida;

    }
    
    public static function diasEntreDatas($dataInicial,$dataFinal){

        if(strlen($dataInicial) <=3 || strlen($dataFinal) <=3){
               return 0;
        }

        $dataFinal = self::dateFormatToPt($dataFinal);

        $inicial = self::dataToArray($dataInicial);
        $final = self::dataToArray($dataFinal);

        $dataI = mktime(0, 0, 0, $inicial['mes'], $inicial['dia'], $inicial['ano']);
        $dataF = mktime(0, 0, 0, $final['mes'], $final['dia'], $final['ano']);

        $diferenca = ($dataF - $dataI);

        return round(($diferenca/60/60/24));

    }

    /**
     * faz paginação
     * @param Array $params Parametros vindos da resquisição, _GET _POST
     * @param SMVC_EntityAbstract $entidade
     * @param int $rpp Registros por pagina
     * @param String $paginaAtual url da pagina em execução
     * @param String $imgProx caminho do icone de imagem do botão avançar pagina
     * @param String $imgProxDisabled caminho do icone de imagem do botão avançar pagina desabilidato
     * @param String $imgAnt caminho do icone de imagem do botão voltar pagina
     * @param String $imgAntDisabled caminho do icone de imagem do botão voltar pagina desabilitado
     * @param String|Array[opcional] $where clausula where
     * @param String $pegarador ? ou &
     * @param String[opcional] $order clausula Order
     * @param int[opcional] quantidade de paginas amostra
     *
     * @return Array
     *  $saida['registros'] :   quantidade de registros
        $saida['controller']:   String html dos botões de paginação
        $saida['total']     :   Total de registros
        $saida['final']:        numero do ultimo registro da pagina atual.
     *  $saida['inicio']:       numero do primeiro registro da pagina atual. Exemplo para inicio = 20: exibindo 20 a 30 de 500 registros
     *  $saida['ant']:          apenas o botão anterior
     *  $saida['prox']:         apenas o botão proximo
     */
    public static function paginacao(
        $params,
        APP_TableAbstract $entidade,
        $rpp,
        $paginaAtual,
        $where = null,
        $order = null,
        $divAlvo = null,
        $imgProx = '/images/next.gif',
        $imgProxDisabled = '/images/next_d.gif',
        $imgAnt = '/images/prev.gif',
        $imgAntDisabled = '/images/prev_d.gif',
        $separador = '/',
        $QtdPaginasMostra = null,
        $imgFirst = '/images/first.png',
        $imgLast = '/images/last.png',
        $imgFirstDisabled = '/images/first_d.png',
        $imgLastDisabled = '/images/last_d.png'
        ){
            
          $pagina = null;

          $paginas = 0;
         
          $inicio = 0;

        foreach ($params as $key => $value) {
            ${$key} = $value;
        }


        if($QtdPaginasMostra == null){
            $QtdPaginasMostra = 3;
        }
         
        if($pagina>0){
            $reduz= $pagina-1;
            $inicio = $reduz*$rpp;
        }
        


        $rTotal = $entidade->getTotalRegistros($where);
        $total = $rTotal[0]['total'];
        
        $registros = $entidade->listaRegistros($where,$order,$rpp,$inicio);
        
        if(is_a($registros, "Zend_Db_Table_Rowset")){
            $registros = $registros->toArray();
        }
        
        $qtdRegistros = count($registros);
       
        //se ultrapassar o rrp(registros por gagina)
        if($rpp<$total){
           
            //recuprera total de paginas
            $paginas = $total/$rpp;
            //arredonda valor
            $paginas = ceil($paginas);
            //armazena referencia para o laço for
            $ref = $paginas+1;

            for ($i = 1 ; $i < $ref ; $i++) {

                //se for pagina atual
                if($pagina == $i){
                    $iLbel = '<strong style="color: blue;">'.$i.'</strong>';
                }else{
                    $iLbel = $i;
                }

                $limiteAnt = $pagina - $QtdPaginasMostra;
                $limiteProx = $pagina + $QtdPaginasMostra;

                if($i > $limiteAnt && $i<$limiteProx){

                    $href = $paginaAtual.$separador."pagina{$separador}".$i;

                    $link = ($divAlvo != '' ? 'javascript:carregar(\''.$href.'\');' : $href);

                     //se for pagina atual
                    if($pagina == $i){
                        $link ="javascript:void(0);";
                    }

                    $botores[] ='<a href="'.$link.'">'.$iLbel.'</a>
                            ';
                }


            }
        }

        if(!empty($botores)){
            $botoesSaida = implode($botores, ' ');

        }

        $frente = $pagina+1;
        $tras   = $pagina-1;

        if(!$pagina){
            $frente = 2;
        }

       //se estiver na ultima pagina

        if($pagina == $paginas){
            $prox = '
                    <a href="javascript:void(0);">
                        <img src="'.$imgProxDisabled.'" title="próxima" alt="próxima" />
                    </a>';
        }else{//se não exibe proximo

            $href = $paginaAtual.$separador."pagina{$separador}".$frente;

             $link = ($divAlvo != '' ? 'javascript:carregar(\''.$href.'\');' : $href);

            $prox= '
                    <a href="'.$link.'">
                        <img src="'.$imgProx.'" title="próxima" alt="proxima" />
                    </a>';
        }

        //se for a primeira pagina

        if($pagina < 2){
            $ant = '
                   <a href="javascript:void(0);">
                        <img src="'.$imgAntDisabled.'" alt="anterior" />
                   </a>';
        }else{//se não exibe anterior

            $href = $paginaAtual.$separador."pagina{$separador}".$tras;

             $link = ($divAlvo != '' ? 'javascript:carregar(\''.$href.'\');' : $href);

            $ant= '
                   <a href="'.$link.'">
                        <img src="'.$imgAnt.'" title="anterior" alt="anterior" />
                   </a>';
        }

        if($imgFirst == null) $imgFirst = 'images/first.png';
        if($imgFirstDisabled == null) $imgFirstDisabled = 'images/first_d.png';

        if($imgLast == null) $imgLast = 'images/last.png';
        if($imgLastDisabled == null) $imgLastDisabled = 'images/last_d.png';


        if($pagina == 1){

             $btnInicio= '
                   <a href="javascript:void(0);">
                        <img src="'.$imgFirstDisabled.'" title="primeira" alt="primeira" />
                   </a>';

        }else{

            $href = $paginaAtual.$separador."pagina{$separador}1";

            $link = ($divAlvo != '' ? 'javascript:carregar(\''.$href.'\');' : $href);

            $btnInicio= '
                   <a href="'.$link.'">
                        <img src="'.$imgFirst.'" title="primeira" alt="primeira" />
                   </a>';
        }


        if($pagina == $paginas){
            $btnFim= '
                   <a href="javascript:void(0);">
                        <img src="'.$imgLastDisabled.'" title="anterior" alt="anterior" />
                   </a>';
        }else{

            $href = $paginaAtual.$separador."pagina{$separador}".$paginas;

             $link = ($divAlvo != '' ? 'javascript:carregar(\''.$href.'\');' : $href);

            $btnFim= '
                   <a href="'.$link.'">
                        <img src="'.$imgLast.'" title="última" alt="última" />
                   </a>';
        }



        //se não precisar de paginação
        if($qtdRegistros>=$total){
            $ant = '';
            $prox = '';
        }
        
        $controlador = '';
        
        if($paginas >=1){

        $controlador =   "
                        <style type='text/css'>


                        .pag-control img{
                            position: relative;
                        }

                        .pag-control{
                            margin: 5px;
                            padding: 1px;
                        }

                        .pag-control a{
                            border: 1px solid #AAAAAA;
                            border-radius: 2px 2px 2px 2px;
                            box-shadow: 0 1px 2px #DDDDDD;
                            color: #555555;
                            margin: 5px;
                            padding: 2px;
                            text-decoration: none;
                            font-family: sans-serif;
                            background: -moz-linear-gradient(center top , #FFFFFF, #CCCCCC) repeat scroll 0 0 transparent;
                        }
                        </style>

                        <div class='pag-control'>
                            {$btnInicio}{$ant}{$botoesSaida}{$prox}{$btnFim}
                        </div>";
        }


        if($inicio == 0){
              $inicioSaida = 1;
        }else{
            $inicioSaida = $inicio+1;
        }

        if(!is_array($registros)){
            $registros = array();
        }

        $saida['registros']    = $registros;
        $saida['ant']          = $ant;
        $saida['prox']         = $prox;
        $saida['controlador']   = $controlador;
        $saida['total']        = $total;
        $saida['final']        = $qtdRegistros+$inicio;
        $saida['inicio']       = $inicioSaida;

        return $saida;

    }

         /**
      * Recupera o conteudo da url
      */
     public static function getUrl($url,$post = array()){

        // Aqui entra o action do formul�rio - pra onde os dados ser�o enviados
        $cURL = curl_init($url);
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);

        // Iremos usar o m�todo POST
        curl_setopt($cURL, CURLOPT_POST, true);
        // Definimos quais informa��es ser�o enviadas pelo POST (array)
        curl_setopt($cURL, CURLOPT_POSTFIELDS, $post);


        $resultado = curl_exec($cURL);

        curl_close($cURL);

        return $resultado;

    }

     public static function getClientName(){

        $server = $_SERVER['SERVER_NAME'];

        $array = explode('.', $server);

        return ($array[0] != 'www' ? $array[0] : $array[1]);

    }

     public static function chaveEncode($str,$chave = "|*&)+@"){

         if(trim($str) == ''){
             return $str;
         }

         return str_replace("=", "", base64_encode($chave.$str));

     }

     public static function chaveDecode($str,$chave = "|*&)+@"){

         if(trim($str) == ''){
             return $str;
         }

         $strTemp = base64_decode($str);


         return str_replace($chave, "", $strTemp);

     }
     
     /**
      * Retorna código hexadecinal da cor de acordo com o percentual passado.
      */
     public static function getColorPercentual($percent){
        
         switch ($percent){
            case $percent <= 20:
                $color = '#ff6633';
            break;

            case $percent <= 30:
                $color = '#ff6600';
            break;

            case $percent <= 50:
                $color = '#ff9900';
            break;

            case $percent <= 70:
                $color = '#66cc00';
            break;

            default :
                $color = '#009900';
            break;
        
        }
        
        return $color;
         
     }

     public static function getAppName(){
        
        $config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/application.ini', 'staging');
        return $config->app->name;
    }   
}