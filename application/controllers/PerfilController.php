<?php

class PerfilController extends APP_ControllerAbstract
{
    
    
       
    public function afterSave(){
       
       $param = $this->getAllParams();
       
       $id = ($param['id'] > 0 ? $param['id'] : $this->lastInsertId);
       
       $permissao = new Application_Model_Permissao();
       
       if(!empty($param['action_item'])){
           
            foreach ($param['action_item'] as $idAction => $allowed) {
                $permissao->salvaPermissao($idAction,$allowed,0, $id);
                $idsAction[] = $idAction;
            }
            
           $whereUpdate = array("id_action NOT IN (".implode(", ", $idsAction).")",
                                           "id_perfil = {$id}");
                                                                   
            $permissao->update(array("allowed"=>"false"),
                                     $whereUpdate);
            
       }else{
           //se desmarcar tudo, seta tudo para false
           $whereUpdate = array("id_perfil = {$id}");
           $permissao->update(array("allowed"=>"false"),
                                     $whereUpdate);
       }
        
    }
    
    public function formAction(){
        
        $this->_helper->layout->disableLayout();
        
        $params = $this->_getAllParams();
        
        $id = (isset($params['id']) ? $params['id'] : 0);
        
        if(!empty($params['id'])){
           $r = $this->model->fetchRow("{$this->campoId} = {$params['id']}");
           
           if(is_a($r, "Zend_Db_Table_Row")){
               $r = $r->toArray();
           }
           
           $this->view->r = $r;
          
           $permissao = new Application_Model_Permissao();
           
           $selecionadas = $permissao->fetchAll("id_perfil = {$params['id']}");
           
           $permissoesSelecionadas = array();
           
           foreach ($selecionadas as $rPermissao) {
               $permissoesSelecionadas[$rPermissao->id_action] = $rPermissao->toArray();
           }
           
           $this->view->permissoesSelecionadas = $permissoesSelecionadas;
           
        }
        
        $controllerModel = new Acl_Model_Controller();
        
        $premissoes = $controllerModel->getControllerActionAgrupado(0,$id);
        
        $this->view->permissoes = $premissoes;
        
        $usuario = new Application_Model_Usuario();
        $this->view->usuarioModel = $usuario;
        
        
    }
    

}

