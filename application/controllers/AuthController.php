<?php

class AuthController extends Zend_Controller_Action
{
    
   
    public function indexAction() {
        
       $this->_helper->layout->setLayout('auth');
        
    }
    
    private function loginRoot(){
        
        $auth = new Zend_Session_Namespace('auth');
        
        $params = $this->_getAllParams();
            
        //verifica se está logando como root
        $config = new Application_Model_Config();

        if($config->authRoot($params['login'],$params['senha'])){
            
            
            $usuario = new Application_Model_Usuario();
            
            $dadosUsuario = array("nome"=>"Usuário root",
                                 "usuario"=>"root",
                                 "email"=>"root@app.com.br",
                                 "departamento"=>"TI",
                                 "gestor"=>"",
                                 "ativo"=>"true",   
                                 "entidade"=>"Sede");
            
           $idUsuarioRoot = $usuario->save($dadosUsuario,"usuario");
           
           $auth->usuario = $dadosUsuario['usuario'];
           $auth->nome = $dadosUsuario['nome'];
           $auth->email = $dadosUsuario['email'];
           $auth->departamento = $dadosUsuario['departamento'];
           $auth->gestor = "";
           $auth->entidade = $dadosUsuario['entidade'];
           $auth->is_root = true;
           $auth->id_usuario = $idUsuarioRoot;
           
           return true;
           
        }
        
        return false;
    }
    
    public function loginAction(){
        
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
        
        try {
            
            $params = $this->_getAllParams();
            
            //verifica se está logando como root
            $isRoot = $this->loginRoot();
            
            
            $usuario = new Application_Model_Usuario;
            
            $dadosUsuario = $usuario->getUsuario($params['login'], $params['senha']);
            
            if(!isset($dadosUsuario->id) && !$isRoot){
                throw new Exception(utf8_decode("Login e/ou senha inválidos."));
            }
            
            if(isset($dadosUsuario->id) && !$isRoot){
                
                
                $auth = new Zend_Session_Namespace('auth');
                
                $auth->usuario = $dadosUsuario['usuario'];
                $auth->nome = $dadosUsuario['nome'];
                $auth->email = $dadosUsuario['email'];
                $auth->departamento = $dadosUsuario['departamento'];
                $auth->gestor = $dadosUsuario['gestor'];
                $auth->entidade = $dadosUsuario['entidade'];
                $auth->is_root = false;
                
                $auth->id_usuario = $dadosUsuario->id;
                
                $controllerModel = new Acl_Model_Controller();
           
                $permissoesUsuarioLogado = $controllerModel->getControllerActionAgrupado($dadosUsuario->id);
               
                $permissoesAgrupadas = array();
               
                foreach ($permissoesUsuarioLogado['modules'] as $idModule => $rModule) {
                    
                    foreach ($permissoesUsuarioLogado['controllers'][$idModule] as $idController => $rController) {
                    
                        foreach ($permissoesUsuarioLogado['actions'][$idModule][$idController] as $idAction => $rAction) {
                              $permissoesAgrupadas[$rModule['module']][$rController['controller']][$rAction['action']] = $rAction;
                        }

                    }
                }
                
                
                $usuario->storePermissionsInCache($dadosUsuario->id, $permissoesAgrupadas);
                
                
            }
            
            
            
            
            $json = array(
                        "msg"=>array("cod"=>0,
                                     "txt"=>"ok")
                            );
            
            
        } catch (Exception $e) {
            
            $json = array(
                        "msg"=>array("cod"=>1,
                                     "txt"=> utf8_encode($e->getMessage()))
                            );
        }
        
        header('Content-Type: application/json');
        
        echo json_encode($json);
        
    }
    
    public function logoutAction(){
        
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
        
        $this->limpaCacheDb();
        $auth = new Zend_Session_Namespace('auth');
        $usuario = new Application_Model_Usuario();
        $usuario->cleanPermissionsFromCache($auth->id_usuario);
        $auth->unsetAll();
        $this->getResponse()->setRedirect('/auth');
        
    }
    
    private function limpaCacheDb(){
        $usuario = new Application_Model_Usuario();
        $usuario->cleanCacheDb();
        
    }

}

