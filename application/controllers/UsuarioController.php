<?php

class UsuarioController extends APP_ControllerAbstract
{

    public function beforeSave()
    {

        $param = $this->getAllParams();

        if (!isset($param['ativo'])) {
            $this->dados['ativo'] = "false";
        } else {
            $this->dados['ativo'] = "true";
        }
        
        if(!empty($param['senha']) 
        && $param['senha'] != $param['senha_r']){
            throw new Exception("As senhas não conferem");
        }
        
        if(empty($param['senha'])){
            unset($this->dados['senha']);
        }else{
            $this->dados['senha'] = sha1($param['senha']);
        }
        
       
        
    }

    /**
     * Atualiza as permissões do usuário logado
     */
    public function afterSave()
    {
        
        $param = $this->getAllParams();
         
        $id = ($param['id'] > 0 ? $param['id'] : $this->lastInsertId);
        
        $permissao = new Application_Model_Permissao();

        if (!empty($param['action_item'])) {

            foreach ($param['action_item'] as $idAction => $allowed) {
                $permissao->salvaPermissao($idAction, $allowed, $id);
                $idsAction[] = $idAction;
            }
            
            
            $permissao->update(array("allowed" => "false"), array("id_action NOT IN (" . implode(", ", $idsAction) . ")",
            "id_usuario = {$id}"));
           
            
        }
        
        $controllerModel = new Acl_Model_Controller();
        $usuario = new Application_Model_Usuario();

        $auth = new Zend_Session_Namespace('auth');
       
        $usuario->cleanPermissionsFromCache($auth->id_usuario);
        
        if (!$auth->is_root) {
            $permissoesUsuarioLogado = $controllerModel->getControllerActionAgrupado($auth->id_usuario);
            //modules
            $permissoesAgrupadas = array();
            
            foreach ($permissoesUsuarioLogado['modules'] as $idModule => $rModule) {
                
                foreach ($permissoesUsuarioLogado['controllers'][$idModule] as $idController => $rController) {

                   foreach ($permissoesUsuarioLogado['actions'][$idModule][$idController] as $idAction => $rAction) {
                       
                       $permissoesAgrupadas[$rModule['module']][$rController['controller']][$rAction['action']] = $rAction;
                   }
                }   
            }
            
            $usuario->storePermissionsInCache($auth->id_usuario, $permissoesAgrupadas);
            
        }
    }

    public function indexAction()
    {

        $usuario = new Application_Model_Usuario();

        if (!$usuario->isAllowed($this->view->controller, $this->view->action)) {
            throw new Exception("Sem permissão para acessar esse recurso.");
        }


        $params = $this->_getAllParams();
        $paramStr = '';
        $where = array();

        if (!isset($params['campo'])) {
            $params['campo'] = '';
        }

        if (!isset($params['filtro'])) {
            $params['filtro'] = '';
        }

        //não mostra o usuário root.
        $where[] = "usuario <> 'root'";

        if(!empty($params['filtro']) && !empty($params['campo'])){
            
            if(is_numeric($params['filtro'])){
                $where[] = $this->model->getAdapter()->quoteInto("{$params['campo']} = ?", $params['filtro']);
            }else if(is_string($params['filtro'])){
                $where[] = $this->model->getAdapter()->quoteInto("retira_acentuacao({$params['campo']}) ILIKE retira_acentuacao(?)", "{$params['filtro']}%");
            }
            
            $paramStr = "/campo/{$params['campo']}/filtro/{$params['filtro']}";
            
        }
        
        $pageModule = ($params['module'] != 'default' ? "/{$params['module']}" : "");

        $paginacao = APP_Util::paginacao($params, $this->model, 10, "{$pageModule}/{$params['controller']}/{$params['action']}" . $paramStr, $where, "{$this->campoId} DESC");

        $this->view->paginacao = $paginacao;
        $this->view->campo = $params['campo'];
        $this->view->filtro = $params['filtro'];
        $this->view->usuarioModel = $usuario;
    }

    public function formAction()
    {

        $this->_helper->layout->disableLayout();

        $params = $this->_getAllParams();

        $id = (isset($params['id']) ? $params['id'] : 0);

        if (!empty($params['id'])) {
            $r = $this->model->fetchRow("{$this->campoId} = {$params['id']}");

            if (is_a($r, "Zend_Db_Table_Row")) {
                $r = $r->toArray();
            }

            $this->view->r = $r;

            $permissao = new Application_Model_Permissao();

            $selecionadas = $permissao->fetchAll("id_usuario = {$params['id']}");

            $permissoesSelecionadas = array();

            foreach ($selecionadas as $key => $rPermissao) {
                $permissoesSelecionadas[$rPermissao->id_action] = $rPermissao->toArray();
            }

            $this->view->permissoesSelecionadas = $permissoesSelecionadas;
        }

        $controllerModel = new Acl_Model_Controller();

        $premissoes = $controllerModel->getControllerActionAgrupado($id);
       
        $this->view->permissoes = $premissoes;

        $perfilModel = new Application_Model_Perfil();

        $this->view->perfils = $perfilModel->fetchAll();
    }

    public function getPermissoesAction()
    {

        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        try {

            $params = $this->getAllParams();

            $permissao = new Application_Model_Permissao();

            $selecionadas = $permissao->fetchAll("id_perfil = {$params['id_perfil']}");

            $permissoesSelecionadas = array();

            foreach ($selecionadas as $key => $rPermissao) {
                $permissoesSelecionadas[$rPermissao->id_action] = $rPermissao->toArray();
            }

            $json = array(
                "msg" => array("cod" => 0,
                    "txt" => "OK."),
                "permissoes" => $permissoesSelecionadas);
        } catch (Exception $e) {

            $json = array(
                "msg" => array("cod" => 1,
                    "txt" => $e->getMessage()),
                "permissoes" => array()
            );
        }

        header('Content-Type: application/json');

        echo json_encode($json);
    }
    
    
    public function formAlterarSenhaAction() {
         $auth = new Zend_Session_Namespace('auth');
//         print_r($auth->id_usuario);
         $this->view->id_usuario = $auth->id_usuario;
         $this->view->login = $auth->usuario;
         $this->view->nome = $auth->nome;
         $this->view->email = $auth->email;
         
    }
    
    public function alterarSenhaAction() {
                
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
        
        

        try {
            
            $auth = new Zend_Session_Namespace('auth');
            
            if ($auth->is_root) {
                throw new Exception("A senha do usuário root não pode ser alterada por esse formulário.");
            }
            
            $params = $this->getAllParams();
            
            $id = $params['id'];
            $senha = $params['senha_antiga_usuario'];
            $senhaNova = sha1($params['senha_nova_usuario']);
            
            $usuario = new Application_Model_Usuario;
            
            $where = array("id = {$id}", "senha = '".sha1($senha)."'");
            
            $retornoSenha = $usuario->fetchRow($where);
            
            if(is_a($retornoSenha, "Zend_Db_Table_Row")){
                $retornoSenha = $retornoSenha->toArray();
            }
            
            if(!isset($retornoSenha['id']) || $retornoSenha['id'] == 0){
                throw new Exception("Senha incorreta!");
            }
            
            if($params['senha_nova_usuario'] != $params['senha_confirm_usuario']){
                throw new Exception("As senhas não conferem!");
            }
            
            $usuario->update(array("senha" => $senhaNova), "id = {$id}");
            
                     
            
            
            
            $json = array(
                "msg" => array("cod" => 0,
                    "txt" => "Senha alterada com sucesso!")  );
        } catch (Exception $e) {

            $json = array(
                "msg" => array("cod" => 1,
                    "txt" => $e->getMessage())
            );
        }

        header('Content-Type: application/json');

        echo json_encode($json);
    }

}
