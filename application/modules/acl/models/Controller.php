<?php

class Acl_Model_Controller extends APP_TableAbstract{
    
    protected $_name = "controller";
    protected $_schema = "acl";
    
    public function getTotalRegistros($where){
        
        $sql = 'SELECT count(1) as total
                FROM acl.controller c
                INNER JOIN acl.module m ON m.id = c.id_module '.$this->montWhere($where);

         $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        
        
        return $dbAdapter->query($sql)->fetchAll();
    }
    
    public function listaRegistros($where,$order,$rpp,$inicio){
        
        $sql = "SELECT c.*,m.nome as modulo
                FROM acl.controller c
                INNER JOIN acl.module m ON m.id = c.id_module ".
                $this->montWhere($where)." ORDER BY {$order} LIMIT {$rpp} OFFSET {$inicio}";
       
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        
        
        return $dbAdapter->query($sql)->fetchAll();
    }
    
    public function getControllerActionAgrupado($idUsuario = 0,$idPerfil = 0){
        
        $sql = "SELECT
                    m.id as module_id,
                    m.nome as module,
                    m.descricao as modulo_desc,
                    c.id as controller_id,
                    c.nome as controller,
                    c.descricao as controller_desc,
                    a.id as action_id,
                    a.nome as action,
                    a.descricao as action_desc
                    ".($idUsuario > 0 || $idPerfil > 0 ? ",p.allowed" : "")."
             FROM acl.controller c
             INNER JOIN acl.action a ON a.id_controller = c.id
             INNER JOIN acl.module m ON m.id = c.id_module
             ".($idUsuario > 0 ? "LEFT JOIN acl.permissao p ON (p.id_action = a.id AND p.id_usuario = {$idUsuario})" : "")."
             ".($idPerfil > 0 ? "LEFT JOIN acl.permissao p ON (p.id_action = a.id AND p.id_perfil = {$idPerfil})" : "")."
             ORDER BY c.descricao,a.id ASC";
        
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        
        $dados = $dbAdapter->query($sql)->fetchAll();
        
        foreach ($dados as $key => $r) {
            $modules[$r['module_id']] = $r;
            $controllers[$r['module_id']][$r['controller_id']] = $r;
            $actions[$r['module_id']][$r['controller_id']][$r['action_id']] = $r;
        }
        
        return array('modules'=>$modules,
                     'controllers'=>$controllers,
                     'actions'=>$actions);
        
    }

}

