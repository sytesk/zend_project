<?php

class Acl_Model_Action extends APP_TableAbstract{
    
    protected $_name = "action";
    protected $_schema = "acl";
    
    public function getTotalRegistros($where){

        $sql = 'SELECT COUNT(1) as total
                 FROM acl.action a
                 INNER JOIN acl.controller c ON c.id = a.id_controller
                 INNER JOIN acl.module m ON m.id = c.id_module '.
                $this->montWhere($where);
        
         $dbAdapter = Zend_Db_Table::getDefaultAdapter();


        return $dbAdapter->query($sql)->fetchAll();
    }
    
    public function listaRegistros($where,$order,$rpp,$inicio){
        
        $sql = "SELECT m.id as id_module,m.nome as module,
                        c.nome as controller,a.*
                 FROM acl.action a
                 INNER JOIN acl.controller c ON c.id = a.id_controller
                 INNER JOIN acl.module m ON m.id = c.id_module ".
                $this->montWhere($where)." ORDER BY {$order} LIMIT {$rpp} OFFSET {$inicio}";
        //echo '<pre>', print_r($sql, true), '</pre>';die();
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        
        
        return $dbAdapter->query($sql)->fetchAll();
    }
}

