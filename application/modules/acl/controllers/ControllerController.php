<?php

class Acl_ControllerController extends APP_ControllerAbstract{

    public function init(){
        
        parent::init();
        
        $auth = new Zend_Session_Namespace('auth');
                
        if(!$auth->is_root){
            throw new Exception("Sem permissão para acessar esse cadastro.");
        }
        
   }
   
   public function formAction(){
        
        $this->_helper->layout->disableLayout();
        
        $params = $this->_getAllParams();
        
        if(!empty($params['id'])){
           $r = $this->model->fetchRow("{$this->campoId} = {$params['id']}");
           
           if(is_a($r, "Zend_Db_Table_Row")){
               $r = $r->toArray();
           }
           
           $this->view->r = $r;
          
        }
        
        $module = new Acl_Model_Module();
        
        $modules = $module->fetchAll();
        
        $this->view->modules = $modules;
        
        $usuario = new Application_Model_Usuario();
        $this->view->usuarioModel = $usuario;
        
    }


}

