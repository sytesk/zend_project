<?php

class Acl_ModuleController extends APP_ControllerAbstract{
    
    public function init(){
        
        parent::init();
        
        $auth = new Zend_Session_Namespace('auth');
                
        if(!$auth->is_root){
            throw new Exception("Sem permissão para acessar esse cadastro.");
        }
        
   }

}

