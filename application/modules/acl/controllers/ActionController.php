<?php

class Acl_ActionController extends APP_ControllerAbstract{

    public function init(){
        
        parent::init();
        
        $auth = new Zend_Session_Namespace('auth');
                
        if(!$auth->is_root){
            throw new Exception("Sem permissão para acessar esse cadastro.");
        }
        
   }
   
   public function indexAction()
    {
        
        $params = $this->_getAllParams();
        $paramStr = '';
        $where = array();
        
        $usuario = new Application_Model_Usuario();
        
        if(!$usuario->isAllowed($this->view->controller, $this->view->action,$this->view->module)){
            throw new Exception("Sem permissão para acessar esse recurso.");
        }
        
        
        if(!isset($params['campo'])){
            $params['campo'] = '';
        }
        
        if(!isset($params['filtro'])){
            $params['filtro'] = '';
        }
        
        if(!empty($params['filtro']) && !empty($params['campo'])){
            
            if(is_numeric($params['filtro'])){
                $where[] = $this->model->getAdapter()->quoteInto("{$params['campo']} = ?", $params['filtro']);
            }else if(is_string($params['filtro'])){
                $where[] = $this->model->getAdapter()->quoteInto("retira_acentuacao({$params['campo']}) ILIKE retira_acentuacao(?)", "{$params['filtro']}%");
            }
            
            $paramStr = "/campo/{$params['campo']}/filtro/{$params['filtro']}";
            
        }
        
        $pageModule = ($params['module'] != 'default' ? "/{$params['module']}" : "");
        
        $paginacao = APP_Util::paginacao($params, $this->model, 10, "{$pageModule}/{$params['controller']}/{$params['action']}".$paramStr, $where,"m.nome,c.nome,a.nome ASC");
        
        $this->view->paginacao = $paginacao;
        $this->view->campo = $params['campo'];
        $this->view->filtro = $params['filtro'];
        $this->view->usuarioModel = $usuario;
        
    }
   
    public function formAction(){
        
        $this->_helper->layout->disableLayout();
        
        $params = $this->_getAllParams();
        
        if(!empty($params['id'])){
           $dados = $this->model->listaRegistros("a.{$this->campoId} = {$params['id']}","id ASC",10,0);
           
           $r = $dados[0];
           
           if(is_a($r, "Zend_Db_Table_Row")){
               $r = $r->toArray();
           }
           
           $this->view->r = $r;
          
        }
        
        $module = new Acl_Model_Module();
        $modules = $module->fetchAll();
        $this->view->modules = $modules;
        
        $controller = new Acl_Model_Controller();
        $controllers = $controller->fetchAll();
        $this->view->controllers = $controllers;
        
        $usuario = new Application_Model_Usuario();
        $this->view->usuarioModel = $usuario;
        
    }
    
}

