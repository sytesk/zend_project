<?php

class Application_Model_Controller extends APP_TableAbstract{

    protected $_schema = "acl";
    protected $_name = "controller";
    
    public function getControllerActionAgrupado($idUsuario = 0,$idPerfil = 0){
        
        $sql = "SELECT c.id as controller_id,
                    c.nome as controller,
                    c.descricao as controller_desc,
                    a.id as action_id,
                    a.nome as action,
                    a.descricao as action_desc
                    ".($idUsuario > 0 || $idPerfil > 0 ? ",p.allowed" : "")."
             FROM acl.controller c
             INNER JOIN acl.action a ON a.id_controller = c.id
             ".($idUsuario > 0 ? "LEFT JOIN acl.permissao p ON (p.id_action = a.id AND p.id_usuario = {$idUsuario})" : "")."
             ".($idPerfil > 0 ? "LEFT JOIN acl.permissao p ON (p.id_action = a.id AND p.id_perfil = {$idPerfil})" : "")."
             ";
        
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        
        $dados = $dbAdapter->query($sql)->fetchAll();
        
        foreach ($dados as $key => $r) {
            $controllers[$r['controller_id']] = $r;
            $actions[$r['controller_id']][$r['action_id']] = $r;
        }
        
        return array('controllers'=>$controllers,
                     'actions'=>$actions);
        
    }
    
}

