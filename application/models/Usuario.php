<?php

class Application_Model_Usuario extends APP_TableAbstract{

    protected $_schema = "acl";
    protected $_name = "usuario";
     
    
    public function isAllowed($controller,$action,$module = 'default'){
        
       $auth = new Zend_Session_Namespace('auth');
       
       if($auth->is_root){
           return true;
       }
       
       $permissoes = $this->getPermissionsFromCache($auth->id_usuario);
       
       return (isset($permissoes[$module][$controller][$action]['allowed']) ? $permissoes[$module][$controller][$action]['allowed'] : false);
        
    }
    
    public function getUsuario($login,$senha){
        
        $rUsuario = $this->fetchRow(array("usuario = '{$login}'",
                                          "senha = '".sha1($senha)."'")
                                   );
        
        return $rUsuario;
        
    }
    
}

