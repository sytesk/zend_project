<?php

class Application_Model_Permissao extends APP_TableAbstract{

    protected $_schema = "acl";
    protected $_name = "permissao";
    
    public function salvaPermissao($idAction,$allowed, $idUsuario = 0,$idPerfil = 0){
        
        $id = 0;
        
        $where[] = "id_action = {$idAction}";
        if($idUsuario > 0){
            $where[] = "id_usuario = {$idUsuario}";
        }
        
        if($idPerfil > 0){
            $where[] = "id_perfil = {$idPerfil}";
        }
        
        $r = $this->fetchRow($where);
        
        
        
        $dados = array("id_action"=>$idAction,
                       "allowed"=>$allowed);
        
        if($idUsuario > 0){
            $dados["id_usuario"] = $idUsuario;
            $id = $idUsuario;
            $campoUpdate = "id_usuario";
        }

        if($idPerfil > 0){
            $dados["id_perfil"] = $idPerfil;
            $id = $idPerfil;
            $campoUpdate = "id_perfil";
        }
        
        
        
        if(empty($r)){
            
            $this->insert($dados);
        }else if($idAction > 0 && $id > 0){
            $this->update($dados,array("{$campoUpdate} = {$id}","id_action = {$idAction}"));
        }
        
    }
    
}

