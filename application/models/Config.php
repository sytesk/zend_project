<?php

class Application_Model_Config extends APP_TableAbstract{

     public $_name = "config";
     
     
     public function authRoot($usuario,$senha){
         
         $r = $this->fetchRow("parametro = 'senha_root'");
        
         //se não estiver parametro cadastrado para senha root, grava com a senha root padrão
         if(!$r->id){
             $this->insert(array("parametro"=>"senha_root","valor"=>  sha1("senh@root")));
              $r = $this->fetchRow("parametro = 'senha_root'");
         }
        
         return ($usuario == 'root' && $r->valor == sha1($senha));
     }
     
}

