<?php

class Application_Model_Log extends APP_TableAbstract{

    protected $_name = "log_";
    
    protected $_schema = "auditoria";
    
    public function init() {
        
        $this->_name = "log_".date("Y");
        
        $this->initLogTable();
        
        return parent::init();
    }
    
    public function gravarLog($table,$id,$dados,$dadosAntes = null){
        
       $tableId = $this->getTableId($table);
       
       $auth = new Zend_Session_Namespace('auth');
       
       $jsonDadosDepois = json_encode($dados);
       $jsonDadosAntes = json_encode($dadosAntes);
       
       $dadosLog = array("id_tabela"=>$tableId,
                        "id_usuario"=>$auth->id_usuario,
                        "pk_tabela"=>$id,
                        "dados_depois"=> $jsonDadosDepois);
       
       if($dadosAntes != null){
           $dadosLog["dados_antes"] = $jsonDadosAntes;
           if(trim($jsonDadosDepois) != trim($jsonDadosAntes)){
               $dadosLog["operacao"] = "A";
               $this->insert($dadosLog);
           }
           
       }else{
           $dadosLog["operacao"] = "I";
           $this->insert($dadosLog);
       }
       
       
        
    }
    
    private function getTableId($table){
        
        $tabela = new Application_Model_Tabela();
        
        $rTabela = $tabela->fetchRow("nome = '{$table}'");
        
        if(!isset($rTabela->nome)){
            
            $dadosTable = array("nome"=>$table);
            $tabela->insert($dadosTable);
            $tableName = $tabela->getSchemaTableName();
            $db = $tabela->getAdapter();
            $pk = $tabela->getPk();
            $tableId = $db->lastInsertId($tableName, $pk[0]['column_name']);
        }else{
            $tableId = $rTabela->cod;
        }
        
        return $tableId;
        
    }
    
    private function initLogTable(){
        
        $ano = date("Y");
        
        $sql = "SELECT * FROM information_schema.tables 
                WHERE table_schema = 'auditoria'
                AND table_name = 'log_{$ano}'";
        
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        
        $dados = $dbAdapter->query($sql)->fetchAll();
        
        if(empty($dados[0]['table_name'])){
            
            $create = "CREATE TABLE auditoria.log_{$ano}(
                            id serial NOT NULL,
                            id_tabela integer,
                            id_usuario integer,
                            operacao char(1),
                            data_hora_operacao timestamp default 'now',
                            pk_tabela integer,
                            dados_antes text,
                            dados_depois text,
                            CONSTRAINT log_{$ano}_pkey PRIMARY KEY (id),
                            CONSTRAINT fk_log_{$ano}_tabela FOREIGN KEY (id_tabela)
                          REFERENCES auditoria.tabela (id) MATCH SIMPLE
                          ON UPDATE NO ACTION ON DELETE NO ACTION,
                            CONSTRAINT fk_log_{$ano}_usuario FOREIGN KEY (id_usuario)
                          REFERENCES acl.usuario (id) MATCH SIMPLE
                          ON UPDATE NO ACTION ON DELETE NO ACTION
                    );
                    
                    ";
            
            $dbAdapter->query($create);
            
            $createComment1 ="comment on table auditoria.log_{$ano} is 'A cada ano é gerado pela aplicação uma tabela com o ano corrente';";
            $createComment2 ="comment on column auditoria.log_{$ano}.operacao is 'I-inclusão A-alteração E-exclusão';";
            
            $dbAdapter->query($createComment1);
            $dbAdapter->query($createComment2);
            
        }
        
    }

}

